// @ts-nocheck
import { getChannel } from "./discord"

export enum ErrorType {
  UNEXPECTED_REJECTION,
  UNCAUGHT_EXCEPTION,
  PLAY,
  UNKNOWN
}

const IGNORABLE_ERRORS = [
  "Unknown Message",

]

export async function onError(err: Error | null | undefined | {}, type: ErrorType) {
  let error = null

  if (err) {
    if (err.message) {
      if (typeof (err.message) === 'string') {
        error = err
      } else {
        if (typeof (err.stack) === 'string') {
          error = new Error(JSON.stringify(err.message))
          error.stack = err.stack
        }
      }
    }
  }

  if (error) {

    if (IGNORABLE_ERRORS.includes(error.message)) {
      console.error(error.stack)
      return
    }

    if (process.env.DEV_CHANNEL_ID) {
      const channel = getChannel(process.env.DEV_CHANNEL_ID as string)
      if (channel) {
        await channel.send(`Oh no, I crashed! ${err.message} - ${err.stack}`.substring(0, 2000))
      }
    }

    return 
    setTimeout(process.exit, 1000)
  }
}