export async function prompt(prompt: string) {
  const res = await fetch('http://localhost:11434/api/generate', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${process.env.OLLAMA_API_KEY}`
    },
    body: JSON.stringify({
      "model": "vivy:latest",
      "prompt": prompt,
      "stream": false
    })
  })

  return (await res.json()).response
}