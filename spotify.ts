import SpotifyWebApi from 'spotify-web-api-node';
import { Track, getDurationFormatted } from './helpers';
import { getInfoYtdlp, searchWithYtdlp } from './ytdlp';

const spotifyApi = new SpotifyWebApi({
  clientId: '7affaa1e6c58411a97788cfa4c7e4339',
  clientSecret: '4d2380b6ae794893b79af8d098aacafe'
});

refreshToken()

async function refreshToken() {
  try {
    const res = await spotifyApi.clientCredentialsGrant()
    spotifyApi.setAccessToken(res.body.access_token)
    setTimeout(refreshToken, res.body.expires_in * 1000)
  } catch (e) {
    setTimeout(refreshToken, 10000)
  }
}

export interface LookupProgress {
  size: number,
  i: number,
  current: string,
  playlistName: string,
  results: Track[],
  failed: SpotifyTrack[],
  thumb: string
}

export async function getTrackInfoOld(url: string) {
  const linkRegex = /(https:\/\/open.spotify.com\/track\/)?([A-z0-9]+)/.exec(url)
  let trackId = ''

  if (linkRegex) {
    trackId = linkRegex[2]
  } else {
    // Unrecognised format
    return
  }

  const res = await spotifyApi.getTrack(trackId)
  return await searchWithYtdlp(`${res.body.name} ${res.body.artists.map(o => o.name).join(' ')}`)
}

export async function getTrackInfo(url: string) {
  const linkRegex = /(https:\/\/open.spotify.com\/track\/)?([A-z0-9]+)/.exec(url)
  let trackId = ''

  if (linkRegex) {
    trackId = linkRegex[2]
  } else {
    // Unrecognised format
    return
  }

  const res = await spotifyApi.getTrack(trackId)
  return res.body
}

type SpotifyTrack = {
  name: string
  artists: SpotifyApi.TrackObjectFull['artists']
}

async function getAllPlaylistTracks(id: string): Promise<SpotifyApi.TrackObjectFull[]> {
  const initial = await spotifyApi.getPlaylistTracks(id, { limit: 50 })
  const tracks = initial.body.items.map(o => o.track)

  let offsetCount = 50
  if (initial.body.next) {
    while (true) {
      const res = await spotifyApi.getPlaylistTracks(id, { limit: 50, offset: offsetCount })
      tracks.push(...res.body.items.map(o => o.track))
      offsetCount += 50
      if (!res.body.next) break
    }
  }

  return tracks
}

export async function getPlaylistTracks(url: string, onProgress?: (progress: LookupProgress) => any) {
  const linkRegex = /(?:https:\/\/open.spotify.com\/(?:album|playlist|track)\/)?([A-z0-9]+)/.exec(url)
  let id = ''

  if (linkRegex) {
    id = linkRegex[1]
  } else {
    return []
  }

  const progress: LookupProgress = {
    size: 0,
    i: 0,
    current: undefined,
    playlistName: '',
    results: [],
    failed: [],
    thumb: undefined
  }

  let tracks: (SpotifyApi.TrackObjectFull | SpotifyApi.TrackObjectSimplified)[] = []
  if (url.indexOf('/playlist/') > -1) {
    const playlist = await spotifyApi.getPlaylist(id)
    progress.playlistName = playlist.body.name
    playlist.body.tracks.next
    tracks = await getAllPlaylistTracks(id)
    progress.thumb = playlist.body.images?.[0]?.url
  } else if (url.indexOf('/album/') > -1) {
    let res = await spotifyApi.getAlbum(id)
    tracks = res.body.tracks.items
    progress.playlistName = res.body.name
    progress.thumb = res.body.images?.[0]?.url
  }

  try {
    for (let i = 0; i < tracks.length; i++) {
      const o = tracks[i];

      const fullTrack = await spotifyApi.getTrack(o.id)

      progress.size = tracks.length
      progress.i = i + 1
      progress.current = o.name

      progress.results.push({
        cacheAlias: `${o.name.substring(0, 100)} [${o.id}]`,
        title: o.name,
        url: `https://open.spotify.com/track/${o.id}`,
        duration: o.duration_ms / 1000,
        durationFormatted: getDurationFormatted(o.duration_ms / 1000),
        spotify: fullTrack.body,
        songArtists: o.artists.map(o => o.name)
      })

      // const ytResult = await findYoutubeVideoFromSpotifyTrack(o)
      // if (ytResult) {
      //   progress.results.push({ ...ytResult, songName: o.name, songArtists: o.artists.map(o => o.name), spotify: o as any })
      // } else {
      //   console.warn(`Failed to find ${o.name}`)
      //   progress.failed.push(o)
      // }

      if (onProgress != null) {
        onProgress(progress)
      }
    }
  }
  catch (e) {
    console.error(e)
  }

  return progress.results
}
