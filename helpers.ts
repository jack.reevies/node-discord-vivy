import { existsSync, mkdirSync, chmodSync, readdir, stat, createWriteStream } from "fs"
import fetch from "node-fetch"
import { resolve } from "path"
export const wait = (ms: number) => new Promise(resolve => setTimeout(resolve, ms))

export const TIME_SECOND = 1000
export const TIME_MINUTE = TIME_SECOND * 60
export const TIME_HOUR = TIME_MINUTE * 60
export const TIME_DAY = TIME_HOUR * 24

export function getDurationFormatted(duration: number) {
  let mins = 0
  let hours = 0

  while (duration >= 60) {
    if (duration >= (60 * 60)) {
      duration -= (60 * 60)
      hours++
    } else if (duration >= 60) {
      duration -= 60
      mins++
    }
  }

  if (hours > 0) {
    return `${padInterval(hours)}:${padInterval(mins)}:${padInterval(Math.floor(duration))}`
  }
  return `${padInterval(mins)}:${padInterval(Math.floor(duration))}`
}

function padInterval(interval: number) {
  return String(interval).padStart(2, '0')
}

export function shuffleArray(array: any) {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
}

export function currentSongElapsed(playingRanges: { start: number, end?: number }[]) {
  let ms = 0
  for (let i = 0; i < playingRanges.length; i++) {
    const range = playingRanges[i];
    const start = range.start
    const end = range.end || Date.now()
    ms += end - start
  }
  return Math.round(ms / 1000)
}

export function safeFn<T>(fn: () => T) {
  try { fn() }
  catch (e) { }
}

export async function safeAsyncFn<T>(fn: () => Promise<T>) {
  try { return await fn() }
  catch (e) { }
}

export async function ensurePathExists(dir: string) {
  const parts = dir.split('/')
  let builder = ''
  for (let i = 0; i < parts.length; i++) {
    const dirPart = parts[i]
    builder += dirPart + '/'
    if (!existsSync(builder)) {
      mkdirSync(builder)
      chmodSync(builder, '777')
    }
  }
}

export function randomArrayElement(arr: any[]) {
  return arr[random(0, arr.length)]
}

export function random(incMin: number, excMax: number) {
  return Math.floor(Math.random() * (excMax - incMin)) + incMin
}

function walk(dir: string, results: any, done: any) {
  readdir(dir, function (err, list) {
    if (err) return done(err)
    var pending = list.length
    if (!pending) return done(null, results)
    list.forEach(function (file) {
      file = resolve(dir, file)
      stat(file, function (noerr, stat) {
        if (stat && stat.isDirectory()) {
          walk(file, results, function (noerr: any, res: any) {
            results = results.concat(res)
            if (!--pending) done(null, results)
          })
        } else {
          results.push(file)
          if (!--pending) done(null, results)
        }
      })
    })
  })
}

/**
 * Reads all files in this directory and any subdirectories (recursive)
 * @param {String} dir
 * @returns {Promise<[String]>} An array of absolute filenames
 */
export function readDir(dir: string, files: string[]) {
  return new Promise((resolve, reject) => {
    walk(dir, files, (err: any, results: any) => {
      if (err) {
        reject(err)
      } else {
        resolve(results)
      }
    })
  })
}

export interface Track {
  cacheAlias?: string
  title: string
  duration: number
  durationFormatted: string
  url: string
  songName?: string
  songArtists?: string[]
  spotify?: SpotifyApi.TrackObjectFull
  art?: string,
  album?: SpotifyApi.AlbumObjectSimplified
}

export function pipeDownload(fsPath: string, feedUrl: string): Promise<number> {
  return new Promise((resolve, reject) => {
    fetch(feedUrl).then(res => {
      const dest = createWriteStream(fsPath)
      const stream = res.body.pipe(dest)

      if (res.status >= 300) {
        reject(new Error(`status ${res.status}`))
        return
      }
      stream.on('finish', () => {
        const size = res.headers.get('content-length')
        resolve(size ? Number(size) : 0)
      })
      stream.on('error', err => {
        reject(err)
      })
    }).catch(e => {
      reject(e)
    })
  })
}

export async function noFail(fn: () => Promise<any>) {
  try {
    return await fn()
  } catch (e) {
    return
  }
}

export async function getLocalFiles(includeLocal = false) {
  const files = []

  if (includeLocal) {
    try { await readDir(`/local`, files) } catch (e) { console.error(e) }
  }

  try { await readDir(process.env.CACHE_PATH, files) } catch (e) { console.error(e) }

  console.log(`Found ${files.length} files`)
  const exts = ['mp3', 'wav', 'ogg', 'webm', 'm4a', 'mkv', 'mp4', 'avi', 'flac']
  return files.filter(f => exts.includes(f.split('.').pop()?.toLowerCase() || ''))
}

export function joinArr(arr: string[], char: string, maxLength: number) {
  let result = ''
  for (const item of arr) {
    if (result.length + item.length + char.length > maxLength) {
      return result
    }
    result += `${item}${char}`
  }
  return result
}

export function singularInterval(fn: () => void | Promise<void>, ms: number, runImmediately = false) {
  if (runImmediately) {
    fn()
  }

  const timeoutFn = async () => {
    try { await fn() } catch (e) {
      console.error(e)
    }
    setTimeout(timeoutFn, ms)
  }

  setTimeout(timeoutFn, ms)
}