FROM node:18

# We don't need the standalone Chromium
ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true

# Install Google Chrome Stable and fonts
RUN apt-get update && apt-get install -y \
    curl gnupg wget \
    && wget --quiet --output-document=- https://dl-ssl.google.com/linux/linux_signing_key.pub | gpg --dearmor > /etc/apt/trusted.gpg.d/google-archive.gpg \
    && sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list' \
    && apt-get update \
    && apt-get install google-chrome-stable -y --no-install-recommends \
    && rm -rf /var/lib/apt/lists/*

# Install additional dependencies
RUN apt-get update && apt-get install -y \
    fonts-liberation \
    libatk1.0 \
    libatk-bridge2.0 \
    libcups2 \
    libdrm2 \
    libgbm1 \
    libgtk-3-0 \
    libnspr4 \
    libnss3 \
    libx11-xcb1 \
    libxcomposite1 \
    libxdamage1 \
    libxrandr2 \
    xdg-utils \
    libu2f-udev \
    libxshmfence1 \
    libglu1-mesa \
    && rm -rf /var/lib/apt/lists/*

# Install Python 3 and create a virtual environment
RUN apt-get update && apt-get install -y python3 python3-pip python3-venv

# Create a virtual environment for Python packages
RUN python3 -m venv /opt/python_env

# Activate the virtual environment
ENV PATH="/opt/python_env/bin:$PATH"

# Install FFmpeg
RUN apt-get install -y ffmpeg
WORKDIR /app
COPY package.json /app
COPY . /app
RUN npm install
RUN npx tsc
# Activate the virtual environment
RUN /opt/python_env/bin/pip install yt-dlp
CMD node dist/index.js
