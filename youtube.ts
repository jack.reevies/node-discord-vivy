import ytpl = require('ytpl');
import { Track } from './helpers';

export async function getVideos(url: string, cookie: string = '') {
  try {
    const obj = await ytpl(url, {
      limit: Infinity,
      requestOptions: {
        headers: {
          cookie: cookie || process.env.YOUTUBE_COOKIE || ''
        }
      }
    })
    return obj.items.map(video => {
      return {
        cacheAlias: `${video.title.substring(0, 100)} [${video.id}]`,
        title: video.title,
        url: video.shortUrl,
        durationFormatted: video.duration,
        duration: video.durationSec
      } as Track
    })
  } catch (e) {
    // Likely a cookie error
    return []
  }
}
