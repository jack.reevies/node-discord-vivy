import { createClient } from '@deepgram/sdk';
import { ReadStream, createWriteStream } from 'fs';
import { get } from 'https';
import { Readable } from 'stream';
import { pipeline } from 'stream/promises';
import * as PlayHT from 'playht'

export async function speak2(text: string) {
  const res = await fetch('https://api.deepgram.com/v1/speak?model=aura-athena-en', {
    method: 'POST',
    headers: {
      'Content-Type': 'text/plain',
      'Authorization': `Token ${process.env.DEEPGRAM_API}`
    },
    body: text
  })

  return Readable.fromWeb(res.body as any) as any as NodeJS.ReadStream
}

PlayHT.init({
  userId: process.env.PLAYHT_USERID,
  apiKey: process.env.PLAYHT_SECRET,
});

export async function streamAudio(text: string) {
  const voice = 's3://voice-cloning-zero-shot/f9bf96ae-19ef-491f-ae69-644448800566/original/manifest.json'
  // Adelaide: s3://voice-cloning-zero-shot/f9bf96ae-19ef-491f-ae69-644448800566/original/manifest.json
  // Indian: s3://voice-cloning-zero-shot/e5df2eb3-5153-40fa-9f6e-6e27bbb7a38e/original/manifest.json
  // @ts-ignore
  const stream = await PlayHT.stream(text, { voiceEngine: 'PlayDialog', 'voice': voice, 'voiceId': voice, 'voiceId2': voice });
  return stream;
}