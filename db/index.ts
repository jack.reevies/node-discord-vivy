import { startConnection } from "./mongo"
import { initValue } from "./value"

export async function startDbConnection() {
  await startConnection()
  await Promise.allSettled([
    initValue()
  ])
}