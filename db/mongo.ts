import { Document, MongoClient } from "mongodb";

const client = new MongoClient(process.env.MONGO_CONN);
const DB_NAME = "yt"

export function getCollection<T extends Document>(collectionName: string) {
  return client.db(DB_NAME).collection<T>(collectionName)
}

export async function startConnection() {
  await client.connect();
  await client.db(DB_NAME).command({ ping: 1 });
}

export async function getCollections() {
  return await client.db(DB_NAME).collections()
}

export async function ensureCollectionExists(collectionName: string) {
  try { await client.db(DB_NAME).createCollection(collectionName) }
  catch (e: any) {
    if (e.message.includes("Collection already exists")) return
    console.error(e)
  }
}

export async function ensureIndexExists(collectionName: string, indexPropertyName: string) {
  const collection = getCollection(collectionName)
  const indexes = await collection.indexes()
  const hasIdKey = indexes.find(o => o.key[indexPropertyName] || o.key[indexPropertyName] === 0)
  if (!hasIdKey) {
    collection.createIndex({ [indexPropertyName]: 1 })
  }
}