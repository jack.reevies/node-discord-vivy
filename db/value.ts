import { ensureIndexExists, getCollection } from "./mongo"

export type IValue = {
  key: string
  value: string
}

const COLLECTION_NAME = 'value'

export async function initValue() {
  await ensureIndexExists(COLLECTION_NAME, 'key')
}

export function model() {
  return getCollection<IValue>(COLLECTION_NAME)
}

export function getAll() {
  return model().find().toArray()
}

export function get(key: string) {
  return model().findOne({ key: key })
}

export function set(key: string, value: any) {
  return model().updateOne({ alias: key }, { $set: { key, value } }, { upsert: true })
}

export function getModel() {
  return model()
}