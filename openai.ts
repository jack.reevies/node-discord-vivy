import OpenAI from "openai";

const openai = new OpenAI({
  apiKey: process.env.OPENAI_KEY,
});

const messageHistory: Record<string, OpenAI.Chat.Completions.ChatCompletionMessageParam[]> = {}

export const openAi = {
  //system: 'You swear a lot and are rude to everyone. Speak in a thick bogan accent.'
  system: 'You swear a lot and are rude to everyone. Speak in a thick bogan accent.'
}

const system: OpenAI.Chat.Completions.ChatCompletionMessageParam = {
  "role": "system",
  "content": [
    {
      "type": "text",
      "text": openAi.system
    }
  ]
}

function buildMessageHistory(who: string, text: string) {
  if (!messageHistory[who]) {
    messageHistory[who] = []
  }

  messageHistory[who].push({
    "role": "user",
    "content": [
      {
        "type": "text",
        "text": text
      }
    ]
  })

  return [
    system,
    ...messageHistory[who]
  ]
}

export async function create(who: string, text: string) {
  const response = await openai.chat.completions.create({
    model: "gpt-4o-mini",
    messages: buildMessageHistory(who, text),
    response_format: {
      "type": "text"
    },
    temperature: 1,
    max_completion_tokens: 2048,
    top_p: 1,
    frequency_penalty: 0,
    presence_penalty: 0
  });

  console.log(JSON.stringify(response, null, 2))
  return response.choices[0].message.content
}