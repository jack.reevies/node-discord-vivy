import { VoiceConnection, AudioPlayer, createAudioPlayer, VoiceConnectionStatus, createAudioResource, AudioPlayerStatus } from '@discordjs/voice'
import fetch from "node-fetch"
import { currentSongElapsed, ensurePathExists, pipeDownload, random, randomArrayElement, TIME_MINUTE, wait } from './helpers';
import { Queue, QueueItem, Type } from './queue';
import { createReadStream, createWriteStream, renameSync, existsSync, statSync, unlinkSync, ReadStream } from 'fs'
import commandQueue from './commands/queue';
import { getChannel, getServerNameOfChannel, removePlayer, sendMessage } from './discord';
import { MessageAttachment, VoiceChannel } from 'discord.js';
import sanitize from 'sanitize-filename'
import { PassThrough, Readable } from 'stream'
import { findInCache, getNameForCache, loadFromCache } from './cache';
import { streamytdlp } from './ytdlp';
import { getSpotifyStream, togglePlayPause } from './spotifyWeb';
import { convertToMp3, createFfmpegStream } from './ffmpeg';
import { getBandcampStream } from './bandcamp';
const scdl = require('soundcloud-downloader').default

interface CancelToken {
  nonce: number,
  stop: boolean,
  pause: boolean
  seek: number
  inject: string
}

const cowbell = 'cowbell.mp3'

let memeSounds = []

export class Player {
  youtubeCookie: string = process.env.YOUTUBE_COOKIE || ''
  playingRanges: { start: number, end?: number }[] = []
  cancelToken: CancelToken = this.createCancelToken()
  audioPlayer: AudioPlayer
  connection: VoiceConnection
  queue: Queue
  invokedFromChannelId: string
  private saveTimeout: NodeJS.Timeout | null = null
  private aloneSince: number = 0
  loopEnabled: boolean = false
  nowPlaying: QueueItem | null = null
  neverAlone = false
  memeMode = false
  private nextMemeAt: number = Date.now() + 1000 * 60 * 60 * 24 * 365 // Never
  private forceMemeIndex: number = -1
  cowbellEnabled: boolean = false
  localOnly = false

  constructor(connection: VoiceConnection, textChannelId: string, queue?: QueueItem[], history?: QueueItem[], id?: string) {
    this.queue = new Queue(this.youtubeCookie, queue, history, id)

    this.invokedFromChannelId = textChannelId
    this.audioPlayer = createAudioPlayer()

    this.audioPlayer.on('error', err => {
      // TODO Need to get better logging!
      // console.error(`An error occured playing stream - please dont crash me`)
    })

    this.audioPlayer.on('stateChange', (oldState, newState) => {
      if (newState.status === AudioPlayerStatus.Playing) {
        this.playingRanges.push({ start: Date.now() })
      } else if (this.playingRanges.length) {
        this.playingRanges[this.playingRanges.length - 1].end = Date.now()
      }
    })

    this.connection = connection

    this.connection.subscribe(this.audioPlayer);

    this.connection.on('stateChange', (oldState, newState) => {
      // TODO Need to get better logging!
      // console.log(`Connection transitioned from ${oldState.status} to ${newState.status}`);
    });

    this.watchQueue()
    this.precacheQueue()
    this.watchForMeme()

    setInterval(() => {
      const channel = getChannel(this.connection.joinConfig.channelId || '') as VoiceChannel
      if (!channel) return

      if (channel.members.size > 1) {
        this.aloneSince = 0
      } else if (!this.aloneSince) {
        this.aloneSince = Date.now()
      }

      if (this.isLonely() && !process.env.NEVER_LONELY && !this.neverAlone) {
        // Idle for a long time - lets get outta here
        sendMessage(this.invokedFromChannelId, "Left due to inactivity. Send a command to keep the party going!")
        this.endSession()
      }
    }, 1000 * 15)
  }

  endSession() {
    this.connection.disconnect()
    this.connection.destroy()
    this.queue.destroy()
    removePlayer(this.invokedFromChannelId)
  }

  private isLonely() {
    return this.aloneSince && Date.now() - this.aloneSince > 1000 * 60 * Number(process.env.IDLE_TIMEOUT_MINUTES || "15")
  }

  private createCancelToken() {
    return {
      nonce: Math.round(Math.random() * 10000),
      pause: false,
      stop: false
    } as CancelToken
  }

  private getSemiRandomMeme() {
    const arr = [...Array(40).fill(1), ...Array(50).fill(2), ...Array(10).fill(3)]
    const rnd = randomArrayElement(arr)
    if (rnd === 1) {
      return randomArrayElement(memeSounds.filter(o => o.startsWith('og')))
    }
    if (rnd === 2) {
      return randomArrayElement(memeSounds.filter(o => o.startsWith('fart')))
    }
    if (rnd === 3) {
      return randomArrayElement(memeSounds.filter(o => o.startsWith('rare')))
    }
    return randomArrayElement(memeSounds)
  }

  async playOrInjectAMeme(index: number = -1) {
    let memeId = index < 0 || index >= memeSounds.length ? this.getSemiRandomMeme() : memeSounds[index]
    memeId = memeId.replace(/(\.mp3|\.ogg|\.wav)$/g, '')
    const track = await loadFromCache(memeId)
    if (!track || !track.cacheAlias) return
    const stream = await this.findCacheForStream2(track.cacheAlias)
    if (!stream) return
    const soundsToVolume = {
      '[fart12] [fart12]': 2,
      '[fart8] [fart8]': 2,
    }
    const vol = soundsToVolume[track.cacheAlias] || 1.5

    if (this.audioPlayer.state.status === 'playing') {
      this.cancelToken.inject = track.cacheAlias
    } else {
      this.playStreamAndWait(stream, track.cacheAlias, true, this.createCancelToken(), vol)
    }
  }

  async playOrInject(file: string, stream: NodeJS.ReadStream) {
    if (this.audioPlayer.state.status === 'playing') {
      this.cancelToken.inject = file
    } else {
      this.playStreamAndWait(stream, file, true, this.createCancelToken())
    }
  }

  async playAMeme(index: number = -1) {
    let memeId = index < 0 || index >= memeSounds.length ? this.getSemiRandomMeme() : memeSounds[index]
    memeId = memeId.replace(/(\.mp3|\.ogg|\.wav)$/g, '')
    const track = await loadFromCache(memeId)
    if (!track || !track.cacheAlias) return
    const stream = await this.findCacheForStream2(track.cacheAlias)
    if (!stream) return
    this.cancelToken = this.createCancelToken()
    const soundsToVolume = {
      '[fart12] [fart12]': 2,
      '[fart8] [fart8]': 2,
    }
    const vol = soundsToVolume[track.cacheAlias] || 1.5
    this.playStreamAndWait(stream, track.cacheAlias, true, this.cancelToken, vol)
  }

  async playMeme(name: string) {
    const track = await loadFromCache(name)
    if (!track || !track.cacheAlias) return
    const stream = await this.findCacheForStream2(track.cacheAlias)
    if (!stream) return
    const soundsToVolume = {
      '[fart12] [fart12]': 2,
      '[fart8] [fart8]': 2,
      '[bumpercars] [bumpercars]': 2,
      '[10points] [10points]': 0.4,
      '[microwave] [microwave]': 2,
      '[schnibblyschnap] [schnibblyschnap]': 2,
    }
    const vol = soundsToVolume[track.cacheAlias] || 1.5
    //this.playStreamAndWait(stream, track.cacheAlias, true, this.cancelToken, vol)
    this.playOrInject(track.cacheAlias, stream)
  }

  private async moreCowbell() {
    const track = await loadFromCache(cowbell.replace(/(\.mp3|\.ogg|\.wav)$/g, ''))
    if (!track || !track.cacheAlias) return
    const stream = await this.findCacheForStream2(track.cacheAlias)
    if (!stream) return
    this.cancelToken = this.createCancelToken()
    await this.playStreamAndWait(stream, track.cacheAlias, true, this.cancelToken)
  }

  private async precacheQueue() {
    while (true) {
      await wait(1000)

      console.time('precacheQueueIteration')
      const queue = this.queue.viewQueue()
      for (const track of queue) {

        if (this.queue.getNowPlaying().cacheAlias === track.cacheAlias || track.hasCache) {
          continue
        }

        const existName = getNameForCache(track.cacheAlias, track.type)
        if (existName && existsSync(existName)) {
          //console.log(`Skipping ${track.cacheAlias} because it's already in the cache`)
          track.hasCache = true
          continue
        }

        let remoteStream: Readable
        if (track.type === Type.YOUTUBE) {
          remoteStream = await streamytdlp(track.uri) as any as NodeJS.ReadStream
        } else if (track.type === Type.SOUNDCLOUD) {
          remoteStream = await scdl.download(track.uri) as any as NodeJS.ReadStream
        } else if (track.type === Type.BANDCAMP) {
          remoteStream = await getBandcampStream(track.uri) as any as NodeJS.ReadStream
        } else {
          //console.log(`Skipping ${track.uri} because its neither from youtube nor soundcloud`)
          continue
        }

        const fileName = getNameForCache(track.cacheAlias, track.type)
        const fileStream = createWriteStream(`${fileName}.partial`)

        await new Promise((resolve) => {
          remoteStream.pipe(fileStream)

          remoteStream.on('end', () => {
            fileStream.end()
            fileStream.close()
            if (statSync(`${fileName}.partial`).size > 1024 * 50) {
              renameSync(`${fileName}.partial`, fileName)
            }
            console.log(`Finished caching queue item ${track.uri} to file as ${fileName}`)
            track.hasCache = true
            remoteStream.destroy()
            resolve(null)
          })
        })
      }
      console.timeEnd('precacheQueueIteration')
    }
  }

  async getStream(nowPlaying: QueueItem) {
    let debugScreenMsg
    let isLocal = true
    let stream: NodeJS.ReadStream | undefined = nowPlaying.cacheAlias ? await this.findCacheForStream2(nowPlaying.cacheAlias) : undefined

    if (!stream) {
      isLocal = false
      if (nowPlaying.type === Type.YOUTUBE) {
        stream = await streamytdlp(nowPlaying.uri) as any as NodeJS.ReadStream
      } else if (nowPlaying.type === Type.SOUNDCLOUD) {
        stream = await scdl.download(nowPlaying.uri)
      } else if (nowPlaying.type === Type.BANDCAMP) {
        stream = await getBandcampStream(nowPlaying.uri) as any as NodeJS.ReadStream
      } else if (nowPlaying.type === Type.SPOTIFY) {
        const baseMsg = `[WIP] Requesting from ${nowPlaying.alias} spotify - please expect a 10-15 second delay. I will work on getting faster, please be patient :point_right: :point_left:`
        debugScreenMsg = await sendMessage(this.invokedFromChannelId, baseMsg)
        // stream = (await getSpotifyStream(nowPlaying.uri, false, undefined, msg => spotifyMsg.edit(`${baseMsg}\n${msg}`))).stream as any as NodeJS.ReadStream
        stream = (await getSpotifyStream(nowPlaying.uri, false, undefined, msg => debugScreenMsg.edit({ files: [new MessageAttachment(Buffer.from(msg.replace(/^data:image\/png;base64,/, ""), 'base64'), 'image.png')] }))).stream as any as NodeJS.ReadStream
      }
    }

    return { stream, isLocal, debugScreenMsg }
  }

  private async watchForMeme() {
    while (true) {
      await wait(1000)
      if (Date.now() >= this.nextMemeAt) {
        await this.playOrInjectAMeme(this.forceMemeIndex)
        this.nextMemeAt = this.memeMode ? getNextRandomMemeTime() : new Date(2030, 1, 1).getTime()
        console.log(`Next meme for ${getServerNameOfChannel(this.connection.joinConfig.channelId || '')} at ${new Date(this.nextMemeAt).toString()}`)
      }
    }
  }

  private async watchQueue() {
    while (true) {
      await wait(1000)

      if (this.connection.state.status != VoiceConnectionStatus.Ready) {
        // We need to do something here but idk what yet
        continue
      }

      while (this.cowbellEnabled) {
        await this.moreCowbell()
      }

      const nowPlaying = this.queue.getNowPlaying()
      if (!nowPlaying) {
        // if (this.memeMode && Date.now() >= this.nextMemeAt) {
        //   await this.playAMeme(this.forceMemeIndex)
        //   this.nextMemeAt = getNextRandomMemeTime()
        //   console.log(`Next meme for ${getServerNameOfChannel(this.connection.joinConfig.channelId || '')} at ${new Date(this.nextMemeAt).toString()}`)
        // }
        continue
      }

      this.nowPlaying = nowPlaying
      this.cancelToken = this.createCancelToken()

      try {
        let { stream, isLocal, debugScreenMsg } = await this.getStream(nowPlaying)

        commandQueue(this, this.invokedFromChannelId)

        await this.playStreamAndWait(stream as NodeJS.ReadStream, nowPlaying.cacheAlias, isLocal, this.cancelToken, undefined, nowPlaying.type)
        setTimeout(() => debugScreenMsg?.delete(), 10 * 1000)

        if (this.nowPlaying === nowPlaying) {
          // This may not always align, for example if the queue is cleared and more songs are lined up
          this.queue.removeFirst()
        }
        if (!this.queue.queue.length && this.loopEnabled && this.queue.history.length) {
          this.queue.queue = this.queue.history
          this.queue.history = []
        }
      }
      catch (e: any) {
        if (e.message.indexOf('Cookie header') > -1 || e.message.indexOf("Status code: 410") > -1) {
          // Cookie has expired
          // I don't know what we do here since its potentially inside the context of another server
        }
        console.error(e)
        sendMessage(this.invokedFromChannelId, `Something went wrong - ${e.message?.substring(0, 1000) || e.toString().substring(0, 1000)}`)
      }
    }
  }

  async findCacheForStream2(cacheAlias: string) {
    if (!process.env.CACHE_PATH) return

    const match = findInCache(cacheAlias)

    if (match && existsSync(match)) {
      return createReadStream(match) as unknown as NodeJS.ReadStream
    }
  }

  playStreamAndWait(stream: NodeJS.ReadStream, cacheAlias: string | undefined, isLocal: boolean, cancelToken: CancelToken, volumeOverride?: number, source?: Type, startAt?: number, sfx?: string) {
    return new Promise(async (resolve, reject) => {

      let debugHasAudioPlayerStarted = false

      this.audioPlayer.once(AudioPlayerStatus.Playing, () => {
        // Theres a weird bug sometimes where the stream ends before starting
        // The AudioPlayerStatus.Idle never triggers because it never started
        debugHasAudioPlayerStarted = true
      })

      let steamEnded = false

      stream.once('end', () => {
        console.log('Stream ended')
        steamEnded = true
      })

      if (process.env.CACHE_PATH) {
        // Keeping this logic together for now because of the stream.destroy at the end
        if (!existsSync(process.env.CACHE_PATH || '')) {
          ensurePathExists(process.env.CACHE_PATH || '')
        }

        if (!isLocal && cacheAlias) {
          // Add in something that disables caching for 10 hour videos
          const fileName = getNameForCache(cacheAlias, source)
          const fileStream = createWriteStream(`${fileName}.partial`)
          stream.pipe(fileStream)

          stream.on('end', () => {
            fileStream.end()
            fileStream.close()
            if (statSync(`${fileName}.partial`).size > 1024 * 50) {
              renameSync(`${fileName}.partial`, fileName)
            }
            console.log(`Finished writing to file`)
            stream.destroy()
            if (!debugHasAudioPlayerStarted) {
              // When the stream ends before anything plays through vivy
              this.playingRanges = []
              resolve(null)
            }
          })
        }
      }

      console.log(`DEBUG: Playing '${cacheAlias}' (isLocal: ${isLocal}) in "${getServerNameOfChannel(this.connection.joinConfig.channelId || '')}" (starting at ${startAt || 0} seconds)`)

      const sfxUrl = sfx ? existsSync(sfx) ? sfx : await findInCache(sfx) : undefined
      const copyStream = createFfmpegStream({ seconds: startAt || 0, volume: volumeOverride || 0, sfxFilePath: sfxUrl })

      //DEBUG
      //setInterval(() => console.log(this.currentSongElapsed()), 1000)

      const nonce = Math.random() * 1000000
      copyStream.stderr.on('data', data => {
        console.error(`${nonce}: ${data.toString()}`)
      })

      copyStream.on('exit', code => {
        console.log(`${nonce}: ffmpeg exited with code ${code}`)
      })

      stream.pipe(copyStream.stdin)
      const audioStream = createAudioResource(copyStream.stdout)

      this.audioPlayer.play(audioStream)

      stream.on('error', err => {
        console.error(err)
        resolve(err)
      })

      const onIdle = async () => {
        // When the song ends naturally
        if (source === Type.SPOTIFY && !steamEnded) {
          console.log(`Restarting connection as stream has not ended`)
          await wait(1000)
          const copyStream = new PassThrough()
          stream.pipe(copyStream)
          this.audioPlayer.play(createAudioResource(copyStream, { inlineVolume: !!volumeOverride }))
          this.audioPlayer.once(AudioPlayerStatus.Idle, onIdle)
          return
        }

        this.playingRanges = []
        console.log(`AudioPlayer idle`)
        resolve(null)
      }

      this.audioPlayer.once(AudioPlayerStatus.Idle, onIdle)

      await this.watchCancelToken(cancelToken)
      stream.destroy()

      if (cancelToken.seek || cancelToken.inject) {
        let { stream } = await this.getStream(this.nowPlaying)
        const start = Math.max(this.currentSongElapsed() + (cancelToken.seek || 0), 0)
        this.playingRanges = [{ start: Date.now() - start * 1000 }]
        const sfx = cancelToken.inject
        cancelToken.seek = undefined
        cancelToken.inject = undefined
        await this.playStreamAndWait(stream, cacheAlias, isLocal, cancelToken, volumeOverride, source, start, sfx)
      }

      resolve(null)
    })
  }

  private async watchCancelToken(cancelToken: CancelToken) {
    const isActive = () => cachedNonce === this.cancelToken.nonce
    const cachedNonce = this.cancelToken.nonce
    while (isActive()) {
      await wait(100)
      if (cancelToken.stop) {
        this.audioPlayer.stop()
        cancelToken.stop = false
        return
      } else if (cancelToken.pause) {
        this.audioPlayer.pause()
        if (this.nowPlaying.type === Type.SPOTIFY) {
          await togglePlayPause()
        }
        while (cancelToken.pause && isActive()) {
          await wait(100)
        }
        this.audioPlayer.unpause()
        if (this.nowPlaying.type === Type.SPOTIFY) {
          await togglePlayPause()
        }
      } else if (cancelToken.seek || cancelToken.inject) {
        return cancelToken
      }
    }
  }

  currentSongElapsed() {
    return currentSongElapsed(this.playingRanges)
  }

  skipCurrentSong() {
    this.cancelToken.stop = true
  }

  pauseCurrentSong() {
    this.cancelToken.pause = true
  }

  resumeCurrentSong() {
    this.cancelToken.pause = false
  }

  seekCurrentSong(seconds: number) {
    this.cancelToken.seek = seconds
  }

  setNewCookie(cookie: string) {
    this.youtubeCookie = cookie
    this.queue.ytCookie = cookie
  }

  resetQueuesAndRegnerateId() {
    this.queue.clearQueueAndFirst()
    setTimeout(() => {
      this.queue = new Queue(this.youtubeCookie)
    }, 1)
  }

  viewQueue() {
    const queue = this.loopEnabled ? this.queue.viewQueueAndHistory() : this.queue.viewQueue()
    if (queue.length && this.nowPlaying && this.nowPlaying?.alias !== queue[0].alias) {
      queue.splice(0, 0, this.nowPlaying)
    }
    return queue
  }

  renameQueue(newId: string) {
    this.queue.renameQueue(newId)
  }

  toggleLoop(override: "on" | "off") {
    if (!override) {
      this.loopEnabled = !this.loopEnabled
      return this.loopEnabled
    }
    this.loopEnabled = override === "on"
    return this.loopEnabled
  }

  toggleMemeMode(override: string) {
    const number = Number(override.replace("force ", ""))
    if (override.startsWith("force")) {
      this.nextMemeAt = 0
      this.forceMemeIndex = isNaN(number) ? -1 : number
      return true
    }

    this.nextMemeAt = getNextRandomMemeTime()
    if (!override) {
      this.memeMode = !this.memeMode
      return this.memeMode
    }
    this.memeMode = override.toLowerCase() === "on"
    return this.memeMode
  }

  needsMoreCowbell(override?: string) {
    const newSetting = override !== "off"
    if (newSetting === this.cowbellEnabled) return this.cowbellEnabled
    this.cowbellEnabled = newSetting
    this.cancelToken.stop = true

    return this.cowbellEnabled
  }

  getNextMemeAt() {
    return this.nextMemeAt
  }
}

function getNextRandomMemeTime() {
  return Date.now() + random(TIME_MINUTE * 5, TIME_MINUTE * 20)
}

async function getMemes() {
  const res = await fetch('https://github.com/JackReevies/vivy-static/raw/main/memes.txt')
  const raw = await res.text()
  const json = raw.split(raw.includes('\r\n') ? '\r\n' : '\n')
  return json.slice(0, json.length - 1)
}

export async function precacheResources() {
  const tempMemeSounds = await getMemes()
  memeSounds = tempMemeSounds.map(o => o.substring(0, o.lastIndexOf('.')))

  const arr = [...tempMemeSounds, cowbell]

  for (const sound of arr) {
    const url = `https://raw.githubusercontent.com/JackReevies/vivy-static/refs/heads/main/${sound}`
    const noext = sound.substring(0, sound.lastIndexOf('.'))
    const fileName = getNameForCache(`[${noext}] [${noext}]`, undefined, 'mp3')
    const ext = sound.substring(sound.lastIndexOf('.'))

    if (existsSync(fileName)) continue
    ensurePathExists(process.env.CACHE_PATH)

    console.log(`precaching resource: ${sound}`)
    try {
      await pipeDownload(fileName + '.temp.' + ext, url)
      await convertToMp3(fileName + '.temp.' + ext, fileName)
      unlinkSync(fileName + '.temp.' + ext)
    } catch (e) { }
    // await convertStreamToOgg(Readable.fromWeb((await fetch(url)).body as any), fileName)
  }
}