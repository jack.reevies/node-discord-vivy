import { getDurationFormatted, readDir, shuffleArray } from "./helpers"
import { getInfo, getPlaylistTracks as soundcloudGetPlaylistTracks } from "./soundcloud"
import { getPlaylistTracks as spotifyGetPlaylistTracks, getTrackInfo, LookupProgress } from "./spotify"
import { getVideos as youtubeGetPlaylistTracks } from "./youtube"
import { EventEmitter } from "events"
import { loadFromCache } from "./cache"
import { existsSync } from "fs"
import { readdir } from "fs/promises"
import { basename, extname } from "path"
import { getInfoYtdlp, searchWithYtdlp } from "./ytdlp"
import { getTracksFromBandcamp } from "./bandcamp"
import { ffmpegGetDuration } from "./ffmpeg"
const { getAudioDurationInSeconds } = require('get-audio-duration');

export interface QueueItem {
  alias: string
  cacheAlias: string | undefined
  uri: string
  type: Type
  requester: string
  time: number
  lengthAlias: string
  lengthSeconds: number
  hasCache?: boolean
}

export enum Type {
  SPOTIFY,
  YOUTUBE,
  SOUNDCLOUD,
  BANDCAMP,
  TTS,
  LOCAL
}

export enum EventTypes {
  Add = "add",
  Remove = "remove"
}

export class Queue {
  id: string = Date.now().toString()
  history: QueueItem[] = []
  queue: QueueItem[] = []
  ytCookie: string
  events: EventEmitter = new EventEmitter()

  constructor(cookie: string, initial?: QueueItem[], history?: QueueItem[], id?: string) {
    this.queue = []
    this.ytCookie = cookie
    if (initial?.length) {
      this.queue = initial
    }
    if (history?.length) {
      this.history = history
    }
    if (id) {
      this.id = id
    }
  }

  async addSingleSoundcloud(url: string, requester: string = 'idklol') {
    if (this.findInQueue(url)) {
      return
    }

    const track = await getInfo(url)
    if (!track) return false

    return this.add(url, Type.SOUNDCLOUD, track.title, track.durationFormatted, track.duration, requester, track.cacheAlias)
  }

  async addSoundcloudPlaylist(url: string, requester: string = 'idklol') {
    const tracks = await soundcloudGetPlaylistTracks(url)

    let added = 0
    tracks.forEach(track => {
      if (this.add(track.url, Type.SOUNDCLOUD, track.title, track.durationFormatted, track.duration, requester, track.cacheAlias)) {
        added++
      }
    })
    return added
  }

  async addSingleYT(url: string, requester: string = 'idklol') {
    if (this.findInQueue(url)) {
      return
    }

    let video = await getInfoYtdlp(url)
    if (!video) {
      return false
    }

    return this.add(video.url, Type.YOUTUBE, video.title, video.durationFormatted, video.duration, requester, video.cacheAlias)
  }

  async addSearchedYT(url: string, requester: string = 'idklol') {
    if (this.findInQueue(url)) {
      return
    }

    let video = await searchWithYtdlp(url)
    if (!video) {
      return false
    }

    return this.add(video.url, Type.YOUTUBE, video.title, video.durationFormatted, video.duration, requester, video.cacheAlias)
  }

  async addYTPlaylist(url: string, requester: string = 'idklol') {
    const videos = await youtubeGetPlaylistTracks(url)
    let added = 0
    for (let i = 0; i < videos.length; i++) {
      const o = videos[i];
      if (this.add(o.url, Type.YOUTUBE, o.title, o.durationFormatted, o.duration, requester, o.cacheAlias)) {
        added++
      }
    }
    return added
  }

  async addSpotifyPlaylist(url: string, requester: string = 'idklol', onProgress?: (progress: LookupProgress) => any) {
    let added = 0
    await spotifyGetPlaylistTracks(url, (progress: LookupProgress) => {
      if (onProgress != null) {
        onProgress(progress)
      }

      if (!progress.results.length) return
      const o = progress.results[progress.results.length - 1]

      if (this.add(o.url, Type.SPOTIFY, o.title, o.durationFormatted, o.duration, requester, o.cacheAlias)) {
        added++
      }
    })

    return added
  }

  async addSpotifySingle(url: string, requester: string = 'idklol') {
    const track = await getTrackInfo(url)
    if (!track) return

    const cacheAlias = track.id

    return this.add(url, Type.SPOTIFY, track.name, getDurationFormatted(Math.round(track.duration_ms / 1000)), track.duration_ms / 1000, requester, `${track.name} [${track.id}]`)
  }

  async addSingleFromBandcamp(url: string, requester: string = 'idklol') {
    const track = await getTracksFromBandcamp(url)
    if (!track.length) return

    return this.add(track[0].uri, Type.BANDCAMP, track[0].title, getDurationFormatted(Math.round(track[0].duration)), track[0].duration, requester, `${track[0].title} [${track[0].id}]`)
  }

  async addListFromBandcamp(url: string, requester: string = 'idklol') {
    const tracks = await getTracksFromBandcamp(url)
    if (!tracks.length) return

    for (const track of tracks) {
      this.add(track.uri, Type.BANDCAMP, track.title, getDurationFormatted(Math.round(track.duration)), track.duration, requester, `${track.title} [${track.id}]`)
    }

    return tracks.length
  }

  async addFromLocalFolder(uri: string, requester: string = 'idklol') {
    const dir = this.getDecodedUri(uri)
    if (!existsSync(dir)) {
      return 0
    }
    const files: string[] = []
    try { await readDir(dir, files) }
    catch (e) { return 0 }
    const added = []
    for (const file of files) {
      const res = await this.addFromLocal(file, requester)
      if (res) {
        added.push(res)
      }
    }

    return added.length
  }

  async addFromLocal(uri: string, requester: string = 'idklol') {
    const file = this.getDecodedUri(uri)
    if (!existsSync(file)) {
      return
    }
    try {
      const alias = basename(file, extname(file))
      const secondDuration = Math.round(await ffmpegGetDuration(file))
      const lengthAlias = getDurationFormatted(secondDuration)
      return this.add(file, Type.LOCAL, alias, lengthAlias, secondDuration, requester, alias) || undefined
    } catch (e) {
      console.error(e)
      return
    }
  }

  private getDecodedUri(uri: string) {
    try {
      return decodeURI(uri.replace("file:/", ""))
    } catch (e: any) {
      return e.message
    }
  }

  async addFromCache(identifier: string, requester: string = 'idklol') {
    const cacheItem = await loadFromCache(identifier)
    if (!cacheItem) return

    return this.add(cacheItem.url, Type.LOCAL, cacheItem.title, cacheItem.durationFormatted, cacheItem.duration, requester, cacheItem.cacheAlias)
  }

  add(url: string, type: Type, alias: string, lengthAlias: string, lengthSeconds: number, requester: string, cacheAlias: string | undefined) {
    if (this.findInQueue(alias)) {
      return false
    }
    const newItem: QueueItem = {
      alias: alias || url,
      cacheAlias,
      uri: url,
      type,
      lengthAlias,
      lengthSeconds,
      requester,
      time: Date.now()
    }
    this.queue.push(newItem)
    this.events.emit('add', newItem)
    return newItem
  }

  setCookie(cookie: string) {
    this.ytCookie = cookie
  }

  clearQueue() {
    if (this.queue.length < 2) return
    const removed = this.queue.splice(1, this.queue.length)
    this.events.emit('remove', removed)
  }

  clearQueueAndFirst() {
    if (this.queue.length < 1) return
    const removed = this.queue.splice(0, this.queue.length)
    this.events.emit('remove', removed)
  }

  removeFirst() {
    if (this.queue.length < 1) return
    const removed = this.queue.splice(0, 1)
    this.history = this.history.concat(removed)
    this.events.emit('remove', removed)
  }

  jumpTo(position: number) {
    if (position < 1) return
    const removed = this.queue.splice(0, position - 1)
    this.history = this.history.concat(removed)
    this.events.emit('remove', removed)
  }

  getNowPlaying() {
    return this.queue[0]
  }

  getUpNext() {
    return this.queue[1]
  }

  viewQueue() {
    return JSON.parse(JSON.stringify(this.queue)) as QueueItem[]
  }

  viewHistory() {
    return JSON.parse(JSON.stringify(this.history)) as QueueItem[]
  }

  viewQueueAndHistory() {
    let queue = this.queue.concat(this.history)
    return JSON.parse(JSON.stringify(queue)) as QueueItem[]
  }

  findInQueue(alias: string) {
    return this.queue.find(o => o.alias === alias || o.uri === alias)
  }

  shuffleQueue() {
    const first = this.queue.splice(0, 1)
    shuffleArray(this.queue)
    this.queue.splice(0, 0, first[0])
  }

  renameQueue(newId: string) {
    this.id = newId
  }

  destroy() {
    this.queue = []
    this.history = []
    this.events.removeAllListeners()
  }
}