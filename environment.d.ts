declare global {
  namespace NodeJS {
    interface ProcessEnv {
      YOUTUBE_COOKIE: string
      DISCORD_CLIENT_TOKEN: string
      DEV_CHANNEL_ID: string
      OWNER_ID: string
      MONGO_CONN: string
      CACHE_PATH: string
      NEVER_LONELY: string
      PO_TOKEN: string
      VISITOR_DATA: string
      OAUTH_EXPIRY_DATE: string
      OAUTH_REFRESH_TOKEN: string
      OAUTH_ACCESS_TOKEN: string
      PLAYHT_USERID: string
      PLAYHT_SECRET: string
      SPOTIFY_DOWNLOAD_DIR: string
    }
  }
}

export {}