import { Message } from "discord.js";
import { getDurationFormatted } from "../helpers";
import { Player } from "../player";

async function meme(player: Player, message: Message, command: string) {
  const regex = /^(meme|sfx|fart)/i
  let override = command.replace(regex, "").trim() || 'on'

  if (!message.member) {
    message.reply(`Sorry - I couldn't grab your discord id. Try again later or get someone else to try`)
    return
  }

  if (override === "time") {
    let time = player.getNextMemeAt()
    if (Date.now() > time) {
      time = Date.now()
    }
    message.reply(`Next fart in ${getDurationFormatted(Math.round((time - Date.now()) / 1000))} (use \`${process.env.COMMAND_PREFIX}fart force\` to do one now)`)
    return
  }

  if (player.toggleMemeMode(override)) {
    message.reply(`( ͡° ͜ʖ ͡°)`)
    return
  }
}

export default meme