import { Message } from "discord.js";
import { Player } from "../player";
import { readdir } from "fs/promises";

async function file(player: Player, message: Message, command: string) {
  if (!message.member) {
    message.reply(`Sorry - I couldn't grab your discord id. Try again later or get someone else to try`)
    return
  }

  if (message.member.id !== '339375761029070858') {
    message.reply(`Sorry - This is a priviledged command`)
    return
  }

  const location = command.replace(/file ?/, '') || './'

  const files = await readdir(location)
  // Split into a string array where each string is no more than 1000 chars
  const chunks = files.reduce((acc: string[], file) => {
    if (acc[acc.length - 1].length + file.length > 1000) {
      acc.push(file)
    } else {
      acc[acc.length - 1] += `${file}\r\n`
    }
    return acc
  }, [''])

  for (const chunk of chunks) {
    await message.reply(`\`\`\`${chunk}\`\`\``)
  }
}

export default file