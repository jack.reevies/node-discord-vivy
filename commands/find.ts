import { Message } from "discord.js";
import { Player } from "../player";
import { getCache } from "../cache";

async function find(player: Player, message: Message, command: string) {
  if (!message.member) {
    message.reply(`Sorry - I couldn't grab your discord id. Try again later or get someone else to try`)
    return
  }

  const file = command.replace(/find ?/, '') || './'

  // Find file in files allowing * as wildcards for path matching
  const found = (message.member.id === '339375761029070858' ? getCache().local : getCache().cache).filter(f => new RegExp(file.replace(/\*/g, '.*'), 'i').test(f))

  // Split into a string array where each string is no more than 1000 chars
  const chunks = found.reduce((acc: string[], file) => {
    const append = `${process.env.COMMAND_PREFIX}q file:/${file}\r\n`
    if (acc[acc.length - 1].length + append.length > 1000) {
      acc.push(append)
    } else {
      acc[acc.length - 1] += append
    }
    return acc
  }, [''])

  for (const chunk of chunks) {
    await message.reply(`\`\`\`${chunk}\`\`\``)
  }
}

export default find