import { Message } from "discord.js";
import { Player } from "../player";

async function stop(player: Player, message: Message) {
  player.resetQueuesAndRegnerateId()
  player.skipCurrentSong()
  message.reply(`🛑 Stopped playing and cleared queue 🛑`)
}

export default stop