import { Message } from "discord.js";
import { Player } from "../player";

async function neverLonely(player: Player, message: Message) {
  player.neverAlone = !player.neverAlone
  if (player.neverAlone) {
    message.reply(`🤖 24/7 mode ON 🤖`)
    return
  }
  message.reply(`💩 24/7 mode OFF 💩`)
}

export default neverLonely