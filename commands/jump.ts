import { Message } from "discord.js";
import { Player } from "../player";

async function jump(player: Player, message: Message, command: string) {
  const countRegex = /\d+$/g.exec(command)
  if (!countRegex) {
    message.reply(`Looks like you tried to jump to X but forgot to provide a number!`)
    return
  }
  player.queue.jumpTo(Number(countRegex[0]))
  player.skipCurrentSong()
  message.reply(`↪️ Jumped to ${player.queue.getUpNext().alias}`)
}

export default jump