import { Message, MessageEmbed } from "discord.js";
import { sendMessage } from "../discord";
import { Player } from "../player";
import { LookupProgress } from "../spotify";
import queue from "./queue";

async function playlist(player: Player, message: Message, command: string) {
  const playlistRegex = /^(playlist |play |pl |list |q |p )/i
  const url = command.replace(playlistRegex, "").trim()

  let result: number = 0
  if (url.indexOf("youtube.com") > -1) {
    result = await player.queue.addYTPlaylist(url, message.author.username)
  } else if (url.indexOf("soundcloud.com/") > -1) {
    result = await player.queue.addSoundcloudPlaylist(url, message.author.username)
  } else if (url.indexOf("open.spotify.com") > -1) {
    result = await processSpotifyMessage(player, message, url)
  } else if (url.includes('bandcamp.com')) {
    result = await player.queue.addListFromBandcamp(url, message.author.username)
  } else if (url.startsWith("file:")) {
    result = await player.queue.addFromLocalFolder(url, message.author.username)
  }

  if (result) {
    message.reply(`🎵 Added ${result} song(s) to the queue 🎵`)
  } else {
    message.reply(`⚠️ No songs added (all in queue already?) ⚠️`)
  }
}

async function processSpotifyMessage(player: Player, message: Message, url: string) {
  const progress: LookupProgress = { size: 0, i: 0, current: '', playlistName: 'Unresolved', results: [], failed: [], thumb: '' }
  const embed = createProgressEmbed(progress)
  let sentMessage: Message | undefined = undefined
  let result = await player.queue.addSpotifyPlaylist(url, message.author.username, async (newProgress: LookupProgress) => {
    // const newEmbed = createProgressEmbed(newProgress)
    // if (sentMessage) {
    //   try { await sentMessage.edit({ embeds: [newEmbed] }) }
    //   catch (e) {
    //     // Likely the message got deleted or we don't have permission to post in channel
    //     sentMessage = undefined
    //   }
    // } else {
    //   sentMessage = await sendMessage(message.channelId, { embeds: [embed] })
    // }
  })

  if (sentMessage) {
    try { (sentMessage as Message).delete() } catch (e) { }
  }

  queue(player, player.invokedFromChannelId)

  return result
}

function createProgressEmbed(progress: LookupProgress) {
  const embed = new MessageEmbed()
    .setColor('#0099ff')
    .setTitle(`Analysing ${progress.playlistName}...`)
    .setTimestamp()

  if (progress.thumb) {
    embed.setThumbnail(progress.thumb)
  }

  embed.addFields(
    { name: 'Current', value: progress.current || 'No Track Title?', },
    { name: 'Progress', value: progress.i.toString() || '0', inline: true },
    { name: 'Playlist size', value: progress.size.toString() || '0', inline: true },
    { name: 'Failed', value: progress.failed.length.toString() || '0', inline: true },
    { name: 'Failed tracks', value: progress.failed.map(o => o.name).join('\r\n').substring(0, 1000) || '0' }
  )

  embed.setFooter(`Vivy`, 'https://cdn.discordapp.com/app-icons/884020741014700053/072952fbdd3db844f879db0f9bd36010.png');

  return embed
}

export default playlist