import { Message } from "discord.js";
import { Player } from "../player";

async function here(player: Player, message: Message) {
  player.invokedFromChannelId = message.channelId
  message.reply(`👍 I'll post updates to this channel instead 👍`)
}

export default here