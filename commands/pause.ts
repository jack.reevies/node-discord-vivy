import { Message } from "discord.js";
import { Player } from "../player";

async function pause(player: Player, message: Message) {
  player.pauseCurrentSong()
  message.reply(`⏸️ Paused`)
}

export default pause