import { Message, MessageEmbed } from "discord.js";
import { Player } from "../player";
const packageJson = require('../../package.json')

async function help(player: Player, message: Message) {

  const cmd = process.env.COMMAND_PREFIX

  const embed = new MessageEmbed()
    .setColor('#0099ff')
    .setTitle(`How to use (Command Prefix \`${cmd}\`)`)
    .setTimestamp()
    .setFooter(`Vivy • ${packageJson.version}`, 'https://cdn.discordapp.com/app-icons/884020741014700053/072952fbdd3db844f879db0f9bd36010.png');

  embed.addFields(
    { name: '\u200B', value: `\`${cmd}play URL\`    | Play a single video/track` },
    { name: '\u200B', value: `\`${cmd}list URL\`    | Play the whole playlist` },
    { name: '\u200B', value: `\`${cmd}pause\`       | Pause playing` },
    { name: '\u200B', value: `\`${cmd}resume\`      | Resume playing` },
    { name: '\u200B', value: `\`${cmd}skip\`        | Skip currently playing song` },
    { name: '\u200B', value: `\`${cmd}jump X\`      | Jump to a specific place in the queue` },
    { name: '\u200B', value: `\`${cmd}loop on/off\` | Toggle looping the queue` },
    { name: '\u200B', value: `\`${cmd}shuffle\`     | Shuffle the current queue` },
    { name: '\u200B', value: `\`${cmd}clear\`       | Empty the current queue (but keep playing current song)` },
    { name: '\u200B', value: `\`${cmd}stop\`        | Empty the current queue and stop playing` },
    { name: '\u200B', value: `\`${cmd}help\`        | View this help` },
    { name: 'Wiki', value: '[Commands for Vivy](https://gitlab.com/jack.reevies/node-discord-vivy/-/wikis/How-to-Use)' },
  )
  message.reply({ embeds: [embed] })
}

export default help