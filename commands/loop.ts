import { Message } from "discord.js";
import { Player } from "../player";

async function loop(player: Player, message: Message, command: string) {
  const overrideRegex = /(on|off)$/gi.exec(command)
  const override = overrideRegex ? overrideRegex[0].toLowerCase() : null
  const isLoop = player.toggleLoop(override as ('on' | 'off'))
  if (isLoop){
    message.reply(`Looping ${player.queue.queue.length + player.queue.history.length} songs`)
  } else {
    message.reply(`Disabled looping`)
  }
}

export default loop