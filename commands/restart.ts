import { Message } from "discord.js";

async function restart(message: Message) {
  if (message.author.id !== process.env.OWNER_ID) {
    message.reply(`I don't need to sleep yet!`)
    return
  }
  message.reply(`Restarting shortly...`)
  setTimeout(() => {
    process.exit(1)
  }, 3000)
}

export default restart