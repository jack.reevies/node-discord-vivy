import { Message, MessageEmbed } from "discord.js";
import { Player } from "../player";
import { randomArrayElement } from "../helpers";
import { speak2, streamAudio } from "../tts";
import { pipeline } from "stream/promises";
import { PassThrough, Readable } from "stream";
import { ReadStream } from "fs";
const packageJson = require('../../package.json')

const englishGreetings = [
  'Hello',
  'Hi',
  'Hey',
  'Yo',
  'Sup',
  'Howdy',
  'Greetings',
  'Salutations',
  'Good day',
]

async function join(player: Player, message: Message) {
  player.invokedFromChannelId = message.channelId

  const embed = new MessageEmbed()
    .setColor('#0099ff')
    .setTitle(`${randomArrayElement(englishGreetings)} :D`)
    .setTimestamp()
    .setFooter(`Vivy • ${packageJson.version}`, 'https://cdn.discordapp.com/app-icons/884020741014700053/072952fbdd3db844f879db0f9bd36010.png');


  // const ttsStream = await speak2('Allo guv na')
  // if (ttsStream) {
  //   player.playStreamAndWait(ttsStream, 'join.mp3', true, null, 1.0)
  // }

  try {
    const ttsStream = await streamAudio('')
    const passThrough = new PassThrough()
    ttsStream.pipe(passThrough)
    player.playStreamAndWait(passThrough as any, 'join.mp3', true, player.cancelToken, 1.0)
  } catch (e) {
    console.error(e)
  }
  message.reply({ embeds: [embed] })
}

export default join