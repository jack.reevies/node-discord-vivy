import { Message, User } from "discord.js";
import { Player } from "../player";
import { createWriteStream } from "fs";
import { EndBehaviorType } from "@discordjs/voice";
import { OpusEncoder } from "@discordjs/opus";
import { startPerpetualStream } from "../ffmpeg";
import { WebSocket } from "ws";
import { PassThrough, Readable } from "stream";
import { sendMessage } from "../discord";
import { wait } from "../helpers";
import OpenAI from 'openai';
import * as PlayHT from 'playht'

let ws: WebSocket | null = null
let tempPlayer: Player | null = null

async function agent(player: Player, message: Message) {
  const stream = player.connection.receiver.subscribe(message.member.user.id, {
    end: {
      behavior: EndBehaviorType.Manual
    }
  });

  tempPlayer = player

  const ffmpeg = startPerpetualStream()

  const buffer = [];
  const encoder = new OpusEncoder(48000, 2);

  stream.on("data", chunk => {
    const opus = encoder.decode(chunk)
    buffer.push(opus)
    ffmpeg.stdin.write(opus)
  });

  const passThrough = new PassThrough();
  ffmpeg.stdout.pipe(passThrough);

  startListening(passThrough, message.member.user)

  // Close WebSocket when process is terminated
  process.on("SIGINT", () => {
    closeWebSocket();
    process.exit();
  });

  ffmpeg.stdout.on('data', (data) => {
    console.log(`stdout: ${data.length}`);
  });


  // Handle completion
  ffmpeg.on('close', (code) => {
    console.log(`FFmpeg process exited with code ${code}`);
  });

  stream.once("end", async () => {
    //TODO 
    console.log(`Ended`)
    ffmpeg.stdin.end()
  });
}

async function startListening(passThrough: PassThrough, user: User) {
  ws = new WebSocket("wss://agent.deepgram.com/agent", {
    headers: {
      Authorization: `Token ${process.env.DEEPGRAM_API}`,
    }
  });

  let keepAliveInterval: NodeJS.Timeout | null = null
  ws.on('open', () => {
    console.log('Connected to Deepgram');

    keepAliveInterval = setInterval(() => {
      ws.send(JSON.stringify({ type: "KeepAlive" }));
    }, 1000 * 5)

    ws.send(JSON.stringify({
      type: "SettingsConfiguration",
      agent: {
        listen: {
          model: "nova-2",
        },
        speak: {
          model: "aura-asteria-en",
        },
        think: {
          provider: {
            type: "anthropic",
          },
          model: "claude-3-haiku-20240307",
        },
      },
    }));

    try {
      passThrough.on("data", (chunk) => {
        try { ws.send(chunk); console.log(`Sent ${chunk.length} bytes`) } catch (e) { console.error(e) }
      });

      passThrough.on("end", async () => {
        console.log("Audio stream ended.");
        closeWebSocket();
      });

      passThrough.on("error", (err) => {
        console.error("Stream error:", err.message);
      });
    } catch (error) {
      console.error("Error fetching audio stream:", error.message);
    }

  });

  const passThroughOut = new PassThrough()

  // Handle WebSocket message event
  ws.on("message", async function incoming(data) {
    console.log("Got message: " + data.toString());

    try {
      let response = JSON.parse(data as any);
      console.log("JSON Message: ", data.toString());

      if (response.type === "UserStartedSpeaking") {
      }
    } catch (e) {
      try {
        tempPlayer.playStreamAndWait(passThroughOut as any, `tts-${Date.now()}-listen-${user.username}`, false, tempPlayer.cancelToken, 1.0)
      } catch (e) {
      }
    }

  });

  // Handle WebSocket close event
  ws.on("close", async function close() {
    console.log("WebSocket connection closed.");
    passThrough.removeAllListeners()
    await wait(1000)
    startListening(passThrough, user);
  });

  // Handle WebSocket error event
  ws.on("error", function error(err) {
    console.error("WebSocket error:", err.message);
  });
}

// Gracefully close the WebSocket connection when done
function closeWebSocket() {
  const closeMsg = JSON.stringify({ type: "CloseStream" });
  ws.send(closeMsg);
  ws.close();
}

export default agent