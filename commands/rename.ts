import { Message } from "discord.js";
import { Player } from "../player";

async function rename(player: Player, message: Message, command: string) {
  command = command.replace('rename', '').trim()
  if (!command.length) {
    message.reply(`Looks like you tried to rename playlist but forgot to provide a name!`)
    return
  }
  player.renameQueue(command)
  message.reply(`Renamed current session as ${command}`)
}

export default rename