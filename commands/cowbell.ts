import { Message } from "discord.js";
import { Player } from "../player";

async function cowbell(player: Player, message: Message, command: string) {
  const regex = /^(cowbell|cow)/i
  let override = command.replace(regex, "").trim() || 'on'

  if (!message.member) {
    message.reply(`Sorry - I couldn't grab your discord id. Try again later or get someone else to try`)
    return
  }

  if (player.needsMoreCowbell(override)) {
    message.reply(`🐮🔔 (use \`${process.env.COMMAND_PREFIX}cowbell off\` to disable)`)
    return
  }
}

export default cowbell