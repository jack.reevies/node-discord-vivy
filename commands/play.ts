import { Message } from "discord.js"
import { Player } from "../player"
import { QueueItem } from "../queue"
import playlist from "./playlist"

async function play(player: Player, message: Message, command: string) {
  const playRegex = /^(play|p|q) /i
  const url = command.replace(playRegex, "").trim()
  let errNeedUri = false
  let result: false | QueueItem | undefined = undefined
  if (url.indexOf("youtube.com") > -1) {
    if (url.indexOf('&list=') > -1) {
      // Warn user about playlist if they wanted to use !list instead
      message.reply(`This is part of a playlist, if you intended to queue the whole playlist use !list instead`)
    }
    result = await player.queue.addSingleYT(url, message.author.username)
  } else if (url.indexOf("soundcloud.com/") > -1) {
    if (url.indexOf('/sets/') > -1) {
      // Redirect to the playlist command
      return playlist(player, message, command)
    }
    result = await player.queue.addSingleSoundcloud(url, message.author.username)
  } else if (url.indexOf('https://open.spotify.com/') > -1) {
    if (url.indexOf('/track/') > -1) {
      result = await player.queue.addSpotifySingle(url, message.author.username)
    } else if (url.indexOf('/playlist/') > -1 || url.indexOf('/album/') > -1) {
      // Redirect to the playlist command
      return playlist(player, message, command)
    }
  } else if (url.includes('bandcamp.com')) {
    result = await player.queue.addSingleFromBandcamp(url, message.author.username)
  } else if (url.startsWith("file:/")) {
    result = await player.queue.addFromLocal(url, message.author.username)
  } else {
    // Try finding by cache
    result = await player.queue.addFromCache(url, message.author.username)
    if (!result) {
      result = await player.queue.addSearchedYT(url, message.author.username)
      if (!result) {
        errNeedUri = true
      }
    }
  }

  if (result) {
    message.reply(`🎵 Added ${result.alias} to the queue 🎵`)
  } else if (result === false) {
    if (player.localOnly) {
      message.reply(`⚠️ Local only mode is enabled, cannot play remote URLs ⚠️`)
      return
    }
    message.reply(errorMessage)
  } else if (errNeedUri) {
    message.reply(`⚠️ URI not recognised (must be a youtube/spotify/soundcloud link!) ⚠️`)
  } else {
    message.reply(`⚠️ Song already in the queue ⚠️`)
  }
}

const errorMessage = `Failed to look up video. This could be for a number of reasons:
1. The url you provided is invalid
2. The video has been removed from YouTube
3. The video is age restricted and I need a new oauth grant (!oauth to fix)
4. (Spotify) The track cannot be found on youtube`

export default play