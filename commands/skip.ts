import { Message } from "discord.js";
import { Player } from "../player";

async function skip(player: Player, message: Message) {
  player.skipCurrentSong()
  message.reply(`⏭️ Skipped`)
}
export default skip