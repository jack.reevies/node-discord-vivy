import { Message } from "discord.js";;
import { Player } from "../player";

async function potoken(player: Player, message: Message, command: string) {
  if (command.includes('view')) {
    const reply = await message.reply(`PO_TOKEN: ${process.env.PO_TOKEN}\nVISITOR_DATA: ${process.env.VISITOR_DATA}`)
    setTimeout(() => reply.delete(), 30 * 1000)
    return
  }

  const [poToken, visitorData] = command.split(' ')
  if (!poToken || !visitorData) {
    message.reply(`Usage: ${process.env.COMMAND_PREFIX}potoken <PO_TOKEN> <VISITOR_DATA>`)
    return
  }

  process.env.PO_TOKEN = poToken
  process.env.VISITOR_DATA = visitorData
}

export default potoken