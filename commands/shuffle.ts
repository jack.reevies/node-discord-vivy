import { Message } from "discord.js";
import { Player } from "../player";
import queue from "./queue";

async function shuffle(player: Player, message: Message) {
  player.queue.shuffleQueue()
  queue(player, player.invokedFromChannelId)
}
export default shuffle