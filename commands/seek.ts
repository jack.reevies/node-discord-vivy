import { Message } from "discord.js";
import { Player } from "../player";

async function seek(player: Player, message: Message) {
  const amount = Number(message.content.substring(6))
  if (Number.isNaN(amount)) {
    return await message.reply(`Please provide a number`)
  }

  if (!player.nowPlaying) {
    return await message.reply(`There is nothing playing`)
  }

  player.seekCurrentSong(Number(amount))
  message.reply(`Seeking`)
}

export default seek