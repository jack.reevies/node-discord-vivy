import { Message } from "discord.js";
import { Player } from "../player";
import { Readable } from "stream";
import { wait } from "../helpers";

async function aifart(player: Player, message: Message) {
  const split = message.content.substring(8).split(":")
  await message.reply(`🧻 Brewing farts`)
  const { seconds, sounds } = await requestFart(split[0] || "fart", split[1] || "3", split[2] || "0.3")
  await message.reply(`🧻 Farts ready!`)

  for (let i = 0; i < sounds.length; i++) {
    await player.playStreamAndWait(sounds[i], `aifart-${Date.now()}-${i}`, false, player.cancelToken, 1.0)
    await wait(seconds * 1000 + 2000)
  }
}

async function requestFart(prompt: string = "fart", duration: string = "4", influence: string = "0.3") {
  const body = `{"text":"${prompt}","prompt_influence":${influence},"duration_seconds":${duration}}`
  console.log(body)
  const res = await fetch("https://api.us.elevenlabs.io/sound-generation", {
    "headers": {
      "accept": "*/*",
      "accept-language": "en-GB,en;q=0.6",
      "authorization": `Bearer ${process.env.ELEVENLABS_API}`,
      "cache-control": "no-cache",
      "content-type": "application/json",
      "pragma": "no-cache",
      "priority": "u=1, i",
      "sec-ch-ua": "\"Brave\";v=\"131\", \"Chromium\";v=\"131\", \"Not_A Brand\";v=\"24\"",
      "sec-ch-ua-mobile": "?0",
      "sec-ch-ua-platform": "\"Windows\"",
      "sec-fetch-dest": "empty",
      "sec-fetch-mode": "cors",
      "sec-fetch-site": "same-site",
      "sec-gpc": "1",
      "Referer": "https://elevenlabs.io/",
      "Referrer-Policy": "strict-origin-when-cross-origin"
    },
    body,
    "method": "POST"
  });

  const json = await res.json()
  return {
    seconds: json.sound_generations_with_waveforms[0].sound_generation_history_item.generation_config.generation_settings.duration_seconds,
    sounds: json.sound_generations_with_waveforms.map(o => base64ToStream(o.waveform_base_64))
  }
}

function base64ToStream(base64String) {
  // Decode the base64 string to a Buffer
  const buffer = Buffer.from(base64String, 'base64');

  // Create a new Readable stream
  const stream = new Readable({
    read() {
      // Push the entire buffer at once
      this.push(buffer);
      this.push(null); // Signal end of stream
    }
  });

  return stream;
}

export default aifart