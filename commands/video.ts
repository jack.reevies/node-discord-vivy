import { Message, MessageAttachment } from "discord.js";
import { Player } from "../player";
import { spawn } from "child_process";
const ffmpeg = require("ffmpeg-static")

async function video(player: Player, message: Message) {
  const ffmpegProcess = spawn(ffmpeg, [
    '-i', 'S:\\Portable Apps\\ffmpeg\\bin\\angy.mp4',
    '-vf', `"select='not(mod(t\,1))'"`,
    '-vsync', 'vfr',
    '-f', 'image2',
    'pipe:1'
  ]);

  const msg = await message.reply(`🎥 Extracting frame from video 🎥`)

  ffmpegProcess.stdout.on('data', async (data) => {
    // Convert the image buffer to base64
    const base64Image = Buffer.from(data).toString('base64');
    try { await msg.edit({ files: [new MessageAttachment(Buffer.from(base64Image.replace(/^data:image\/png;base64,/, ""), 'base64'), 'image.png')] }) }
    catch(e) {
      console.error(e)
    }
  });

  ffmpegProcess.stderr.on('data', (data) => {
    console.error(`FFmpeg stderr: ${data}`);
  });

  ffmpegProcess.on('close', (code) => {
    //if (code !== 0) {
      // reject(new Error(`FFmpeg process exited with code ${code}`));
      console.error('Non zero exit code')
    //}
  });
}

export default video
