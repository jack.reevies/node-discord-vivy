import { MessageEmbed, Message, Collection } from "discord.js";
import { clearMyMessagesInChannel, getMessagesInChannel, getServerNameOfChannel, sendMessage } from "../discord";
import { getDurationFormatted, safeAsyncFn } from "../helpers";
import { Player } from "../player";

async function queue(player: Player, channelId: string) {
  const queue = player.queue.viewQueue()
  if (!queue.length) {
    sendMessage(channelId, `Nothing in queue!`)
    return
  }

  try { sendEmbedForQueuePosition(player, channelId, null, 1) }
  catch (e) { }
}

async function isLastMessageQueue(channelId: string) {
  const messages = await getMessagesInChannel(channelId) as Collection<string, Message>
  const mostRecent = messages.first()
  if (!mostRecent || !mostRecent.client.user) return
  if (mostRecent.author.id !== mostRecent.client.user.id) return
  if (mostRecent.embeds[0]?.title?.startsWith("Queue")) {
    return mostRecent
  }
}

async function removeExtraReactions(message: Message) {
  const reactions = message.reactions.cache

  reactions.each(async reaction => {
    (await reaction.users.fetch()).each(user => {
      if (user.id === message.author.id) return
      reaction.users.remove(user)
    })
  })
}

async function sendEmbedForQueuePosition(player: Player, channelId: string, message: Message | null = null, start: number = 1, direction = 1) {
  const { embed, startIndex, endIndex } = embedQueue(player, start, direction)
  let title = `Queue for ${getServerNameOfChannel(channelId)}`
  if (!Number(player.queue.id)) {
    title = `${title} (${player.queue.id})`
  }
  embed.setTitle(title)

  let sentMessage = message || await isLastMessageQueue(channelId)

  if (sentMessage) {
    // We can edit the queue message
    removeExtraReactions(sentMessage)
    sentMessage = await sentMessage.edit({ embeds: [embed] })
  } else {
    clearMyMessagesInChannel(channelId)
    sentMessage = await sendMessage(channelId, { embeds: [embed] })

    if (!sentMessage) {
      return
    }

    try {
      sentMessage.react('◀')
      await sentMessage.react('▶')
    }
    catch (e) {
      console.error(e)
      // WE SHOULD DO SOMETHING HERE BUT I DONT KNOW WHAT
    }
  }

  const filter = (reaction: any, user: any) => {
    return user.id !== sentMessage?.author.id && ['◀', '▶'].includes(reaction.emoji.name);
  };

  try {
    const collected = await sentMessage.awaitReactions({ filter, max: 1 })
    // Display the previous/next embed
    const reaction = collected.first()

    if (reaction?.emoji.name === '◀' && startIndex >= 1) {
      try { sendEmbedForQueuePosition(player, channelId, sentMessage, startIndex, -1) }
      catch (e) { }
    } else if (reaction?.emoji.name === '▶') {
      try { sendEmbedForQueuePosition(player, channelId, sentMessage, endIndex, 1) }
      catch (e) { }
    }
  } catch (e) {
    try { sentMessage.reactions.removeAll() } catch (e) { }
  }
}

function embedQueue(player: Player, start: number = 1, direction = 1) {
  const queue = player.viewQueue()

  const nowPlaying = queue[0]
  const currentElapsed = player.currentSongElapsed()
  const timeLeftAlias = getDurationFormatted(nowPlaying.lengthSeconds - currentElapsed)

  const queueTimeLeft = queue.reduce((acc, val) => acc + val.lengthSeconds, 0) - currentElapsed
  const queueTimeLeftAlias = getDurationFormatted(queueTimeLeft)

  let startIndex = 0
  let endIndex = 0

  if (direction >= 0) {
    startIndex = start
    endIndex = start + 10
  } else {
    endIndex = start
    startIndex = start - 10
  }

  if (startIndex > queue.length - 10) {
    startIndex = queue.length - 10
  }

  if (endIndex > queue.length) {
    endIndex = queue.length
  }

  if (startIndex < 1) {
    startIndex = 1
  }

  const embed = new MessageEmbed()
    .setColor('#0099ff')
    .setTitle('Queue')
    .setTimestamp()

  if (nowPlaying) {
    embed.addFields({ name: 'Currently Playing', value: `\r\n${makeDiscordUrl(nowPlaying.alias, nowPlaying.uri)} | \`${timeLeftAlias} left\` | \`Added by: ${nowPlaying.requester}\`` })
  }

  for (let i = startIndex; i < queue.length; i++) {
    const o = queue[i];
    if (!o) continue
    let value = `\`${i}.\` ${makeDiscordUrl(o.alias, o.uri)} | \`${o.lengthAlias}\` | \`Added by: ${o.requester}\``
    if (i == startIndex) {
      value = `\r\n${value}`
    }
    embed.addFields(
      { name: i === startIndex ? 'Up Next:' : '\u200B', value: value, },
    )
    endIndex = i
    if (embed.fields.length >= 11) {
      break
    }
  }

  embed.setFooter(`${startIndex} - ${endIndex} of ${(queue.length - 1) || 1} items • ETA ${queueTimeLeftAlias} left`, 'https://cdn.discordapp.com/app-icons/884020741014700053/072952fbdd3db844f879db0f9bd36010.png');

  return { embed, startIndex, endIndex }
}

function makeDiscordUrl(alias: string, uri: string) {
  if (!uri.startsWith("http://") && !uri.startsWith("https://")) {
    return `[${truncateAlias(alias)}](http://www.example.com/sorry-local-file-no-url-available)`
  }
  return `[${truncateAlias(alias)}](${uri})`
}

function truncateAlias(alias: string) {
  if (alias.length > 112) {
    return `${alias.substring(0, 109)}...`
  }
  return alias.substring(0, 112)
}

export default queue