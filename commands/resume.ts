import { Message } from "discord.js";
import { Player } from "../player";

async function resume(player: Player, message: Message) {
  player.resumeCurrentSong()
  message.reply(`▶️ Resumed playback`)
}

export default resume