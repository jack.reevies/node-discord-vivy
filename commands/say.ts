import { Message } from "discord.js";
import { Player } from "../player";
import { speak2, streamAudio } from "../tts";
import { PassThrough } from "stream";
import { pipeDownload } from "../helpers";
import { createReadStream, createWriteStream } from "fs";
import { sep } from "path";
import { saveStreamAsMp3 } from "../ffmpeg";

async function say(player: Player, message: Message) {
  const file = await downloadAudio(message.content.substring(message.content.indexOf('say ') + 4))
  const stream = createReadStream(file)
  player.playOrInject(file, stream as any)

  // const ttsStream = await speak2(message.content.substring(message.content.indexOf('say ') + 4))
  // if (ttsStream) {
  //   player.playStreamAndWait(ttsStream,`tts-${Date.now()}`, false, player.cancelToken, 1.0)
  // }
}

function downloadAudio(say: string): Promise<string> {
  return new Promise(async (resolve, reject) => {
    const ttsStream = await streamAudio(say)
    const fileName = `${process.env.CACHE_PATH}${sep}[tts-${Date.now()}].mp3`
    await saveStreamAsMp3(ttsStream as any, fileName)
    resolve(fileName)
    // const writeSteam = createWriteStream(fileName)
    // ttsStream.pipe(writeSteam)

    // ttsStream.on('end', () => {
    //   resolve(fileName)
    // })
  })
}

export default say