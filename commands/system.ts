import { Message } from "discord.js";
import { Player } from "../player";
import { openAi } from "../openai";

async function systen(player: Player, message: Message) {
  if (message.content.endsWith('system')) {
    return await message.reply(`Current system prompt: ${openAi.system}`)
  }

  openAi.system = message.content.substring(message.content.indexOf('system ') + 7)
  await message.reply(`System prompt set to: ${openAi.system}`)
}
export default systen