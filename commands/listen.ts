import { Message, User } from "discord.js";
import { Player } from "../player";
import { createWriteStream } from "fs";
import { EndBehaviorType } from "@discordjs/voice";
import { OpusEncoder } from "@discordjs/opus";
import { startPerpetualStream } from "../ffmpeg";
import { WebSocket } from "ws";
import { PassThrough, Readable } from "stream";
import { onDiscordMessage, sendMessage } from "../discord";
import { wait } from "../helpers";
import OpenAI from 'openai';
import * as PlayHT from 'playht'
import { prompt } from "../ollama";
import { streamAudio } from "../tts";
import { create } from "../openai";

PlayHT.init({
  userId: process.env.PLAYHT_USERID,
  apiKey: process.env.PLAYHT_SECRET,
});

let ws: WebSocket | null = null
let tempPlayer: Player | null = null
let isResponding = false
let keepAliveInterval: NodeJS.Timeout | null = null

async function listen(player: Player, message: Message) {
  const stream = player.connection.receiver.subscribe(message.member.user.id, {
    end: {
      behavior: EndBehaviorType.Manual
    }
  });

  const onMessage = async (newMessage: Message) => {
    if (newMessage.author.id !== message.author.id) {
      return
    }

    respond(newMessage.member.user, newMessage.content)
  }

  onDiscordMessage(onMessage)

  tempPlayer = player

  const ffmpeg = startPerpetualStream()

  const buffer = [];
  const encoder = new OpusEncoder(48000, 2);
  const passThrough = new PassThrough();


  stream.on("data", chunk => {
    const opus = encoder.decode(chunk)
    buffer.push(opus)
    ffmpeg.stdin.write(opus)
  });

  //let fileStream = createWriteStream(`${message.member.user.id}-${Date.now()}.pcm`)

  ffmpeg.stdout.pipe(passThrough);
  //passThrough.pipe(fileStream)

  // setInterval(() => {
  //   fileStream.end()
  //   fileStream.close()

  //   fileStream = createWriteStream(`${message.member.user.id}-${Date.now()}.pcm`)
  //   passThrough.pipe(fileStream)
  // }, 1000 * 30)

  startListening(passThrough, message.member.user)

  // Close WebSocket when process is terminated
  process.on("SIGINT", () => {
    closeWebSocket();
    process.exit();
  });

  ffmpeg.stdout.on('data', (data) => {
    // console.log(`stdout: ${data.length}`);
  });


  // Handle completion
  ffmpeg.on('close', (code) => {
    console.log(`FFmpeg process exited with code ${code}`);
  });

  stream.once("end", async () => {
    //TODO 
    console.log(`Ended`)
    ffmpeg.stdin.end()
  });
}

async function startListening(passThrough: PassThrough, user: User) {
  ws = new WebSocket("wss://api.deepgram.com/v1/listen", {
    headers: {
      Authorization: `Token ${process.env.DEEPGRAM_API}`,
    }
  });



  ws.on('open', () => {
    console.log('Connected to Deepgram');

    keepAliveInterval = setInterval(() => {
      ws.send(JSON.stringify({ type: "KeepAlive" }));
    }, 1000 * 5)

    let lastSent = Date.now()
    let lastChunk

    setInterval(() => {
      if (Date.now() - lastSent > 2000 && lastChunk) {
        ws.send(lastChunk)
      }
    }, 1000 * 2)

    try {
      passThrough.on("data", (chunk) => {
        lastSent = Date.now()
        lastChunk = chunk
        try { ws.send(chunk); } catch (e) { }
      });

      passThrough.on("end", async () => {
        console.log("Audio stream ended.");
        closeWebSocket();
      });

      passThrough.on("error", (err) => {
        console.error("Stream error:", err.message);
      });
    } catch (error) {
      console.error("Error fetching audio stream:", error.message);
    }

  });

  // Handle WebSocket message event
  ws.on("message", async function incoming(data) {
    let response = JSON.parse(data as any);
    console.log("Message: ", data.toString());
    if (response.type === "Results") {
      const result = response.channel.alternatives[0];
      console.log("Transcript: ", response.channel.alternatives[0].transcript);
      if (!result.transcript.length) return

      if (result.transcript.includes('stop') || result.transcript.includes('shut up')) {
        tempPlayer.cancelToken.stop = true
        tempPlayer.cowbellEnabled = false
        return
      }

      if (result.transcript.includes('fart')) {
        tempPlayer.playAMeme()
        return
      }

      // if (result.transcript.match(/ ?cow ?/) || result.transcript.match(/ ?cal ?/)) {
      //   tempPlayer.needsMoreCowbell(tempPlayer.cowbellEnabled ? 'off' : 'on')
      //   return
      // }

      if (tempPlayer.cowbellEnabled && !(result.transcript.match(/ ?cow ?/) || result.transcript.match(/ ?cal ?/))) {
        return
      }

      if (result.transcript.includes('play') && result.transcript.includes('youtube')) {
        const searchTerm = result.transcript.replace('play', '').replace('youtube', '')
        console.log(`Searching for ${searchTerm}`)
      }

      if (isResponding) return

      const sent = await sendMessage(process.env.DEV_CHANNEL_ID, `${user.username} said: ${result.transcript} (${Math.round(result.confidence * 100)}% confident)`)
      const llmResponse = await create(user.id, result.transcript)
      await sent.edit(sent.content + `\n\nResponse: ${llmResponse}`)

      const ttsStream = await streamAudio(llmResponse)
      const passThrough = new PassThrough()
      ttsStream.pipe(passThrough)
      isResponding = true
      await tempPlayer.playStreamAndWait(passThrough as any, `tts-${Date.now()}`, false, tempPlayer.cancelToken, 1.0)
      isResponding = false
      //sendMessage(process.env.BOT_COMMAND_CHANNEL_ID, `${user.username} said: ${result.transcript} (${Math.round(result.confidence * 100)}% confident)`)
    }
  });

  // Handle WebSocket close event
  ws.on("close", async function close() {
    console.log("WebSocket connection closed.");
    passThrough.removeAllListeners()
    await wait(1000)
    startListening(passThrough, user);
  });

  // Handle WebSocket error event
  ws.on("error", function error(err) {
    console.error("WebSocket error:", err.message);
  });
}

async function respond(user: any, says: string) {
  if (isResponding) return

  const llmResponse = await create(user.id, says)
  await sendMessage(process.env.DEV_CHANNEL_ID, `${user.username} said: ${says}\r\nResponse: ${llmResponse}`)

  const ttsStream = await streamAudio(llmResponse)
  const passThrough = new PassThrough()
  ttsStream.pipe(passThrough)
  isResponding = true
  await tempPlayer.playStreamAndWait(passThrough as any, `tts-${Date.now()}`, false, tempPlayer.cancelToken, 1.0)
  isResponding = false
}

// Gracefully close the WebSocket connection when done
function closeWebSocket() {
  const closeMsg = JSON.stringify({ type: "CloseStream" });
  ws.send(closeMsg);
  ws.close();
  clearInterval(keepAliveInterval)
}

export default listen