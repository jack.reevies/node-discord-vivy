import { Message } from "discord.js";
import { Player } from "../player";
import { convertCookieStringToYTDLPFile, streamytdlp } from "../ytdlp";
import { writeFileSync } from "fs";

const exampleAgeRestrictedVideo = 'https://www.youtube.com/watch?v=_ynaX7PtJe0'

async function cookie(player: Player, message: Message, command: string) {
  const youtubeCookie = command.replace("cookie", "").trim()
  // We could verify the cookie as well?
  message.reply(`Thanks! Please hold while I verify...`)

  writeFileSync('cookies.txt', convertCookieStringToYTDLPFile(youtubeCookie))

  const stream = await streamytdlp(exampleAgeRestrictedVideo)

  stream.on('error', (e: Error) => {
    if (e.message.indexOf('Cookie header') > -1) {
      // Nope - cookie was no good
      message.reply(`Sorry - that cookie didnt work either. Full error was: ${e.message}`)
    } else {
      message.reply(`Sorry - still not working. Error: ${e.message}`)
    }
    clearTimeout(successTimeout)
  })
  const successTimeout = setTimeout(() => {
    // If we get here then lets assume the video worked!
    message.reply(`Yum, looks like that cookie works!`)
    stream.destroy()
    player.setNewCookie(youtubeCookie)
  }, 5000)
}

export default cookie