import { Message, MessageEmbed } from "discord.js";
import { sendMessage } from "../discord";
import { Player } from "../player";
import sanitize from "sanitize-filename";
import { sep } from "path";
import { existsSync } from "fs"
import { getListTracks } from "../spotifyWeb";
import { cleanupTempFiles, downloadSpotifyTrack, getTrackFileName } from "../spotifyTasks";

async function download(player: Player, message: Message, command: string) {
  const playlistRegex = /^(download )/i
  const url = command.replace(playlistRegex, "").trim()

  await processSpotifyMessage(player, message, url)
}

async function processSpotifyMessage(player: Player, message: Message, url: string) {
  const progress: Progress = { playlistName: '<Spotify List>', thumb: 'https://misc.scdn.co/liked-songs/liked-songs-300.png', queue: 0, done: 0, active: '', failed: 0 }

  let sentMessage: Message | undefined = undefined

  const newEmbed = createProgressEmbed(progress)

  sentMessage = await sendMessage(message.channelId, {
    embeds: [newEmbed]
  })

  if (url === 'test' || url === 'library') {
    url = 'https://open.spotify.com/collection/tracks'
  }

  const tracks: Awaited<ReturnType<typeof getListTracks>>['tracks'] = []

  const library = await getListTracks(url, tracks)
  progress.playlistName = library.title
  progress.thumb = library.thumb

  const updateInterval = setInterval(async () => {
    if (sentMessage) {
      try { await sentMessage.edit({ embeds: [createProgressEmbed(progress)] }) }
      catch (e) {
        // Likely the message got deleted or we don't have permission to post in channel
        sentMessage = undefined
      }
    } else {
      sentMessage = await sendMessage(message.channelId, { embeds: [createProgressEmbed(progress)] })
    }
  }, 10000)

  progress.queue = library.tracks.length

  for (const track of library.tracks) {
    progress.active = track.title

    const fileName = getTrackFileName(track)
    const completeName = `${process.env.SPOTIFY_DOWNLOAD_DIR}${sep}${sanitize(fileName)}`

    if (existsSync(completeName + '.mp3')) {
      console.log(`Skipping ${track.title} because it already exists`)
      cleanupTempFiles(completeName)
      continue
    }

    try {
      await downloadSpotifyTrack(track, completeName)
      console.log(`Downloaded ${track.title}`)
      progress.done++
    }
    catch (e) {
      console.error(e)
      progress.failed++
    }
  }

  clearInterval(updateInterval)

  if (sentMessage) {
    try { (sentMessage as Message).delete() } catch (e) { }
  }
}

function createProgressEmbed(progress: Progress) {
  const embed = new MessageEmbed()
    .setColor('#0099ff')
    .setTitle(`Downloading ${progress.playlistName}...`)
    .setTimestamp()

  if (progress.thumb) {
    embed.setThumbnail(progress.thumb)
  }

  embed.addFields(
    { name: 'Current', value: progress.active || 'No Track Title?' },
    { name: 'Downloaded', value: progress.done.toString(), inline: true },
    { name: 'Failed', value: progress.failed.toString(), inline: true },
    { name: 'Queued', value: progress.queue.toString(), inline: true }
  )

  embed.setFooter(`Vivy`, 'https://cdn.discordapp.com/app-icons/884020741014700053/072952fbdd3db844f879db0f9bd36010.png');

  return embed
}

type Progress = {
  playlistName: string
  thumb: string
  queue: number
  done: number
  active: string
  failed: number
};

export default download