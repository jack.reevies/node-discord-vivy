import { Message } from "discord.js";
import { Player } from "../player";

async function local(player: Player, message: Message, command: string) {
  player.localOnly = !player.localOnly
  message.reply(`Local only mode is now ${player.localOnly ? "enabled" : "disabled"}`)
}

export default local