import { Message } from "discord.js";
import { getPlayerInServer, getVCConnectionsInServer } from "../discord";
import { ErrorType } from "../error";
import { Player } from "../player";

async function reset(player: Player, message: Message) {
  if (player) {
    player.endSession()
  } else if (message.guildId) {
    // We don't have a reference, this might be because we've lost track somehow or we're legit not in a vc yet
    const player = getPlayerInServer(message.guildId)
    if (!player) {
      // Don't have a reference in code - this shouldnt happen
      // But if it does, kill all voice connections to that server
      endConnectionInUnknownChannel(message.guildId)
      return
    }
    player.endSession()
  } else {
    message.reply("Somehow I have got myself into an unexpected state. This is likely a permission error. I cannot see the guildId for this server.\r\n\r\nI've reported this to the owner...")
  }
}

function endConnectionInUnknownChannel(serverId: string) {
  const voiceConnection = getVCConnectionsInServer(serverId)
  voiceConnection?.destroy()
}

export default reset