import { exec, spawn } from "child_process";
import sanitize from "sanitize-filename";
import { Readable } from "stream";

const ffmpeg = require("ffmpeg-static")
const ffprobe = require("ffprobe-static")

export function ffmpegGetDuration(inputPath: string): Promise<number> {
  return new Promise((resolve, reject) => {
    const command = [
      ffmpeg,
      '-i', `"${inputPath}"`, // Input file
      '-c', 'copy',
      '-f', 'null',
      '-'
    ].join(' ');

    exec(command, (error, stdout, stderr) => {
      if (error) {
        console.error(`exec error: ${error}`);
        console.error(stderr)
        reject(false) // Indicate failure
        return;
      }
      try {
        let regex = /Duration:\s*(\d+):(\d+):(\d+).(\d+)/.exec(stderr) || /Duration:\s*(\d+):(\d+):(\d+).(\d+)/.exec(stdout)

        if (!regex) {
          // Fallback
          const temp = [...stderr.matchAll(/time=\d+:\d+:\d+\.\d+ /g)].pop()[0]
          regex = /time=(\d+):(\d+):(\d+)\.(\d+)/.exec(temp)
        }

        const length = Number(regex[1]) * 60 * 60 + Number(regex[2]) * 60 + Number(regex[3]) + (Number(regex[4].padEnd(3, '0')) / 1000)
        resolve(length);
      } catch (e) {
        reject(new Error(`Failed to find Duration in ffmpeg output: ${stderr}`))
      }
    });
  })
}

export function verifyffmpegLength(inputPath: string, expectedLength: number) {
  // ffmpeg -i file.mp3 -c copy -f null -

  return new Promise((resolve, reject) => {
    const command = [
      ffmpeg,
      '-i', `"${inputPath}"`, // Input file
      '-c', 'copy',
      '-f', 'null',
      '-'
    ].join(' ');

    exec(command, (error, stdout, stderr) => {
      if (error) {
        console.error(`exec error: ${error}`);
        console.error(stderr)
        reject(false) // Indicate failure
        return;
      }
      try {
        const regex = /Duration:\s*(\d+):(\d+):(\d+).(\d+)/.exec(stderr) || /Duration:\s*(\d+):(\d+):(\d+).(\d+)/.exec(stdout);
        const length = Number(regex[1]) * 60 * 60 + Number(regex[2]) * 60 + Number(regex[3]) + (Number(regex[4].padEnd(3, '0')) / 1000)
        if (Math.abs(length - expectedLength) > 5) {
          reject(new Error(`Expected length ${expectedLength} but got ${length} for ${inputPath}`))
        } else {
          resolve(true); // Indicate success
        }
      } catch (e) {
        reject(new Error(`Failed to find Duration in ffmpeg output: ${stderr}`))
      }
    });
  })
}


export function convertOggToMp3(inputPath: string, outputPath: string, tags) {
  // ffmpeg -i in.mp3 -i test.png -map 0:0 -map 1:0 -c copy -id3v2_version 3 -metadata:s:v title="Album cover" -metadata:s:v comment="Cover (front)" out.mp3
  return new Promise((resolve, reject) => {
    const command = [
      ffmpeg,
      '-i', `"${inputPath}"`, // Input file
      '-i', `"${tags.thumb.replace(/\\/g, '/')}"`,
      '-y',
      '-map', '0:a',
      '-map', '1:0',
      '-acodec', 'libmp3lame', // Specify audio codec
      ...buildID3TagsForFfmpeg(tags),
      `"${outputPath}"` // Output file
    ].join(' ');

    console.log(command)

    exec(command, (error, stdout, stderr) => {
      if (error) {
        console.error(`exec error: ${error}`);
        console.error(stderr)
        reject(false) // Indicate failure
        return;
      }
      console.log(`Conversion successful. Converted ${inputPath} to ${outputPath}`);
      resolve(true); // Indicate success
    });
  })
}

function buildID3TagsForFfmpeg(tags: any) {
  const command = []

  if (!tags) {
    return command
  }

  if (tags.title) {
    command.push(`-metadata`, `title="${sanitize(tags.title)}"`)
  }

  if (tags.artist) {
    command.push(`-metadata`, `artist="${sanitize(tags.artist)}"`)
  }

  if (tags.album) {
    command.push(`-metadata`, `album="${sanitize(tags.album)}"`)
  }

  if (tags.year) {
    command.push(`-metadata`, `year="${tags.year}"`)
  }

  if (tags.year) {
    command.push(`-metadata`, `date="${tags.year}"`)
  }

  if (tags.thumb) {
    command.push(`-metadata:s:v`, `title="Album cover"`)
    command.push(`-metadata:s:v`, `comment="Cover (front)"`)
  }

  if (command.length) {
    command.push('-id3v2_version', '4')
  }

  return command
}

export function removeLeadingSilence(inputPath: string, outputPath: string, tags?: any) {
  return new Promise((resolve, reject) => {
    const command = [
      ffmpeg,
      '-i', `"${inputPath}"`, // Input file
      '-af', 'silenceremove=1:0:-60dB',
      '-y',
      ...buildID3TagsForFfmpeg(tags),
      `"${outputPath}"` // Output file
    ].join(' ');

    console.log(command)

    exec(command, (error, stdout, stderr) => {
      if (error) {
        console.error(`exec error: ${error}`);
        console.error(stderr)
        reject(false) // Indicate failure
        return;
      }
      console.log(`Conversion successful. Converted ${inputPath} to ${outputPath}`);
      resolve(true); // Indicate success
    });
  })
}

export function startPerpetualStream(outFormat: string = 'mp3') {
  const command = [
    '-f', 's16le',
    '-ar', '48000',
    '-ac', '2',
    '-i', 'pipe:0',
    '-f', outFormat,
    'pipe:1'
  ]

  return spawn(ffmpeg, command, { stdio: ['pipe', 'pipe'] })
}

export function createFfmpegStream(options: { seconds: number, volume?: number, sfxFilePath?: string }) {
  const command = [
    ...(options.seconds ?
      ['-ss', `${options.seconds || 0}`] : []),
    '-i', 'pipe:0',
    '-vn',
    ...(options.volume ?
      ['-filter:a', `volume=${options.volume}`] : []),
    // '-acodec', 'libmp3lame',
    // '-b:a', '128k',
    '-f', 'mp3',
    ...(options.sfxFilePath ?
      ['-filter_complex', '[0:a]volume=1[v1];[1:a]volume=2[v2];[v1][v2]amix=inputs=2:duration=longest',
        '-i', options.sfxFilePath,
        '-f', 'mp3'] : []),
    'pipe:1'
  ]

  console.log(`Running: ${command.join(' ')}`)
  return spawn(ffmpeg, command, { stdio: ['pipe', 'pipe'] })
}

export async function saveStreamAsMp3(stream: Readable, fileOut: string){
  return new Promise((resolve, reject) => {
    const command = [
      '-i', 'pipe:0',
      '-f', 'mp3',
      '-acodec', 'libmp3lame',
      '-b:a', '128k',
      fileOut
    ]

    const child = spawn(ffmpeg, command, { stdio: ['pipe', 'pipe'] })
    stream.pipe(child.stdin)

    child.stderr.on('data', data => {
      console.error(`${data.toString()}`)
    })

    child.on('exit', (code) => {
      if (code !== 0) {
        console.log(`FFmpeg exited with code ${code}`)
      }
      resolve(fileOut)
    })
  })
}

export async function convertToMp3(fileIn: string, fileOut: string) {
  return new Promise((resolve, reject) => {
    const command = [
      '-i', fileIn,
      '-f', 'mp3',
      '-acodec', 'libmp3lame',
      '-b:a', '128k',
      fileOut
    ]

    const child = spawn(ffmpeg, command, { stdio: ['pipe', 'pipe'] })

    child.stderr.on('data', data => {
      console.error(`${data.toString()}`)
    })

    child.on('exit', (code) => {
      if (code !== 0) {
        console.log(`FFmpeg exited with code ${code}`)
      }
      resolve(fileOut)
    })
  })
}