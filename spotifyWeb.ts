import { Browser, Cookie, Page, PageEvent } from 'puppeteer'
import { launch, getStream } from 'puppeteer-stream'

import puppeteer from 'puppeteer-extra'
import StealthPlugin from 'puppeteer-extra-plugin-stealth'
import { Transform } from 'stream'
puppeteer.use(StealthPlugin())

let browser: Browser

export const currentlyPlaying = {
  stream: null as any,
  url: null as string | null,
  page: null as Page | null,
  songTitle: null as string | null,
  durationSeconds: null as number | null
}

export async function getListTracks(url: string = 'https://open.spotify.com/collection/tracks', tracks: SpotifyTrack[] = []) {
  // @ts-ignore
  browser = await launch(puppeteer, {
    executablePath: process.env.CHROME_LOCATION || '/usr/bin/google-chrome-stable',
    defaultViewport: {
      width: 480,
      height: 360,
    },
    args: ['--no-sandbox', '--auto-open-devtools-for-tabs'],
    //@ts-ignore
    headless: 'new'
  });

  console.log(`Chrome launched`)

  const cookies: Partial<Cookie>[] = [
    { name: 'sp_dc', value: process.env.SPOTIFY_COOKIE, 'domain': '.spotify.com', 'path': '/', 'expires': 2175434943, 'httpOnly': true, 'secure': true },
  ]

  const page = await browser.newPage()
  await page.setRequestInterception(true)

  let headersForLibraryTracks: any = {}
  let bearerToken: string = ''
  let clientToken: string = ''
  let type: SpotifyListType = url.includes('collection/tracks') ? 'library' : url.includes('playlist') ? 'playlist' : 'album'

  page.on('request', async request => {
    if (request.url().startsWith('https://api-partner.spotify.com/pathfinder/v1/query?operationName')) {
      // This is the call to get the first page of tracks
      // We need to mimic this call again but with a different offset
      const headers = request.headers()
      bearerToken = headers['authorization']
      clientToken = headers['client-token']
      headersForLibraryTracks = { ...headers }
      //debugger
    }
    //console.log(await request.url())
    request.continue()
  })

  console.log(`Page opened`)

  await page.setCookie(...cookies as any[])
  await page.goto(url)
  console.log(`Page loaded`)

  while (!bearerToken) {
    await wait(100)
  }

  await wait(5000)

  const id = url.split('/').pop().split('?')[0]

  const metadata = await page.evaluate(type === 'album' ? getAlbumMetadata : getPlaylistMetadata)

  await loadEntirePlaylist(page, clientToken, bearerToken, type, id, 0, tracks)

  await page.close()
  await browser.close()

  return { ...metadata, tracks }
}


export async function togglePlayPause() {
  try {
    return await currentlyPlaying.page.evaluate(playerTogglePausePlay)
  } catch (e) { console.error(e) }
}

export async function getSpotifyStream(url: string, dryRun: boolean = false, onUpdate?: (data: { title: string, duration: number, elapsed: number }) => void, debugFn?: (data: string) => void) {
  // @ts-ignore
  browser = await launch(puppeteer, {
    executablePath: process.env.CHROME_LOCATION || '/usr/bin/google-chrome-stable',
    defaultViewport: {
      width: 1280,
      height: 720,
    },
    args: ['--no-sandbox', '--auto-open-devtools-for-tabs', '--audio-buffer-size=2048', '--force-wave-audio'],
    //@ts-ignore
    headless: 'new'
  });

  debugFn?.(`Chrome launched`)
  console.log(`Chrome launched`)

  const cookies: Partial<Cookie>[] = [
    { name: 'sp_dc', value: process.env.SPOTIFY_COOKIE, 'domain': '.spotify.com', 'path': '/', 'expires': 2175434943, 'httpOnly': true, 'secure': true },
  ]

  const page = await browser.newPage()

  const interval = setInterval(async () => {
    try { debugFn?.(await page.screenshot({ 'fullPage': true, encoding: 'base64' })) } catch (e) { }
  }, 1000)

  //debugFn?.(`Page opened`)
  console.log(`Page opened`)

  await page.setCookie(...cookies as any[])
  await page.goto(url)
  // debugFn?.(`Loading page`)
  console.log(`Loading page`)


  await page.waitForNetworkIdle()
  for (let i = 0; i < 5; i++) {
    await wait(1000)
    // debugFn?.(`Waiting for page load ${'█'.repeat(i)}`)
  }

  if (!dryRun && await page.evaluate(doWeNeedToTakeControl)) {
    // debugFn?.(`Uh-oh: Attempting to take control of another web player...`)
    console.log(`We are not the current player - taking control...`)
    await page.evaluate(takeControlTwo)
    await wait(10000)
  }

  let stream: Transform

  if (!dryRun) {
    //@ts-ignore
    stream = await getStream(page, { audio: true, video: false, mimeType: 'audio/webm' });
    console.log(`Stream created`)
  }

  const { artist, album, year, thumb, title: songTitle } = await page.evaluate(getTrackMetadata)

  console.log(`${songTitle}`)

  currentlyPlaying.stream = stream
  currentlyPlaying.url = url
  currentlyPlaying.songTitle = songTitle
  currentlyPlaying.page = page

  console.log(`Attempting ${songTitle} by ${artist} (${year}) on album ${album} at ${url} (thumbnail: ${thumb})`)

  if (dryRun) {
    clearInterval(interval)
    await page.close()
    await browser.close()
    return { stream, title: songTitle, artist, album, year, thumb }
  }

  // debugFn?.(`Looking for play button`)
  await page.evaluate(clickTrackPlayButton)
  // debugFn?.(`Ta-da? :fingers_crossed:`)
  trackWatcher(onUpdate, debugFn)

  clearInterval(interval)
  return { stream, title: songTitle, artist, album, year, thumb }
}

async function trackWatcher(onUpdate?: (data: { title: string, duration: number, elapsed: number }) => void, debugFn?: (data: string) => void) {
  let elapsed = 0
  let duration = 0
  try {
    await wait(10000)
    let currentTrack = await currentlyPlaying.page.evaluate(getNowPlaying)
    let initialDuration = await currentlyPlaying.page.evaluate(getDuration)

    currentlyPlaying.durationSeconds = initialDuration
    console.log(`now playing: ${currentTrack}`)

    let i = 0
    while (true) {
      await wait(100)
      i++

      const nowPlaying: string = await currentlyPlaying.page.evaluate(getNowPlaying)

      elapsed = await currentlyPlaying.page.evaluate(getElapsed)
      duration = await currentlyPlaying.page.evaluate(getDuration)

      if (i % 10 === 0) {
        console.log(`now playing: ${nowPlaying} || elapsed: ${elapsed}`)
        try { currentlyPlaying.page?.screenshot({ 'fullPage': true, encoding: 'base64' }).then(o => debugFn?.(o)) } catch (e) { }
      }

      if (nowPlaying !== currentTrack) {
        console.log(`No longer playing desired song (${nowPlaying} vs ${currentTrack})`)
        break
      }

      onUpdate?.({ title: nowPlaying, elapsed, duration })
    }
  } catch (e) {
    console.error(e)
  }

  currentlyPlaying.stream.end()
  await currentlyPlaying.stream.destroy();

  await browser.close();

  currentlyPlaying.stream = null
  currentlyPlaying.url = null
  currentlyPlaying.page = null
  currentlyPlaying.songTitle = null
  currentlyPlaying.durationSeconds = null
}

function getNowPlaying() {
  return (document.querySelectorAll(`[data-testid="context-item-link"]`)[0] as any)?.innerText
}

function getElapsed() {
  function getSecondsDurationFromString(str: string) {
    const duration = /(\d+):(\d+)(?::(\d+))?/g.exec(str) || [0, 0, 0]
    return duration[3] ? Number(duration[1]) * 60 * 60 + Number(duration[2]) * 60 + Number(duration[3]) : Number(duration[1]) * 60 + Number(duration[2])
  }

  return getSecondsDurationFromString((document.querySelector('[data-testid="playback-position"]') as any).innerText)
}

function getDuration() {
  function getSecondsDurationFromString(str: string) {
    const duration = /(\d+):(\d+)(?::(\d+))?/g.exec(str) || [0, 0, 0]
    return duration[3] ? Number(duration[1]) * 60 * 60 + Number(duration[2]) * 60 + Number(duration[3]) : Number(duration[1]) * 60 + Number(duration[2])
  }

  return getSecondsDurationFromString((document.querySelector('[data-testid="playback-duration"]') as any).innerText)
}

function clickTrackPlayButton() {
  const ctrl = (document.querySelector('div[data-testid="action-bar"] > div > div > button') as HTMLButtonElement)
  if (ctrl.disabled) throw new Error("Track is unplayable")
  ctrl.click()
}

function playerTogglePausePlay() {
  const ctrl = (document.querySelector('[data-testid="control-button-playpause"]') as HTMLButtonElement)
  ctrl.click()
}

function playerIsPaused() {
  const pauseSvg = `<path d="M2.7 1a.7.7 0 0 0-.7.7v12.6a.7.7 0 0 0 .7.7h2.6a.7.7 0 0 0 .7-.7V1.7a.7.7 0 0 0-.7-.7H2.7zm8 0a.7.7 0 0 0-.7.7v12.6a.7.7 0 0 0 .7.7h2.6a.7.7 0 0 0 .7-.7V1.7a.7.7 0 0 0-.7-.7h-2.6z`
  const playSvg = `<path d="M3 1.713a.7.7 0 0 1 1.05-.607l10.89 6.288a.7.7 0 0 1 0 1.212L4.05 14.894A.7.7 0 0 1 3 14.288V1.713z`
  const ctrl = (document.querySelector('[data-testid="control-button-playpause"] > span > span > svg') as HTMLButtonElement)
  if (ctrl.innerHTML.startsWith(playSvg)) {
    return true
  }
  return false
}

function getTrackDuration() {
  for (const node of document.querySelectorAll('[data-testid="track-page"] .encore-text-body-small')) {
    const regex = /(\d+):(\d+)(?::(\d+))?/g.exec((node as any).innerText)
    if (!regex) continue
    return Number(regex[1]) * 60 + Number(regex[2])
  }
}

function getTrackTitle() {
  return (document.querySelector(`[data-testid="entityTitle"]`) as HTMLElement).innerText
}

function getTrackMetadata() {
  function getSecondsDurationFromString(str: string) {
    const duration = /(\d+):(\d+)(?::(\d+))?/g.exec(str) || [0, 0, 0]
    return duration[3] ? Number(duration[1]) * 60 * 60 + Number(duration[2]) * 60 + Number(duration[3]) : Number(duration[1]) * 60 + Number(duration[2])
  }

  const title = (document.querySelector(`[data-testid="entityTitle"]`) as HTMLElement).innerText
  const img = document.querySelector('[data-testid="track-page"]')?.children?.[0]?.querySelector('img')?.src
  const data = (document.querySelector(`[data-testid="entityTitle"]`).nextElementSibling as HTMLElement).innerText.replace(/\n/g, '').trim().split('•')
  return {
    artist: data[0],
    album: data[1],
    year: data[2],
    seconds: getSecondsDurationFromString(data[3]),
    thumb: img,
    title
  }
}

function getSecondsDurationFromString(str: string) {
  const duration = /(\d+):(\d+)(?::(\d+))?/g.exec(str) || [0, 0, 0]
  return duration[3] ? Number(duration[1]) * 60 * 60 + Number(duration[2]) * 60 + Number(duration[3]) : Number(duration[1]) * 60 + Number(duration[2])
}

function getAlbumMetadata() {
  const title = (document.querySelector(`[data-testid="entityTitle"]`) as HTMLElement).innerText
  const img = document.querySelector('[data-testid="album-page"]')?.children?.[0]?.querySelector('img')?.src
  return {
    thumb: img,
    title
  }
}

function getPlaylistMetadata() {
  const title = (document.querySelector(`[data-testid="entityTitle"]`) as HTMLElement).innerText
  const img = document.querySelector('[data-testid="playlist-page"]')?.children?.[0]?.querySelector('img')?.src
  return {
    thumb: img,
    title
  }
}

function doWeNeedToTakeControl() {
  //@ts-ignore
  return !!Array.from(document.querySelectorAll(`.encore-internal-color-text-base`)).find(o => o.innerText.startsWith("Playing on"))
}

async function takeControlTwo() {
  const wait = (ms) => new Promise(resolve => setTimeout(resolve, ms))

  let start = Date.now()
  while (Date.now() - start < 5000) {
    const found = document.querySelector(`[aria-label="Connect to a device"]`)
    if (found) {
      //@ts-ignore
      found.click()
      break
    }
    await wait(100)
  }

  start = Date.now()
  while (Date.now() - start < 5000) {
    const found = Array.from(document.querySelectorAll("div")).find(o => o.innerText === "Connect to this device")
    if (found) {
      //@ts-ignore
      return found.click()
    }
    await wait(100)
  }
}

async function takeControl(page: Page) {
  await waitForElement(page, () => document.querySelector(`[aria-label="Connect to a device"]`), o => o.click())
  await waitForElement(page, () => Array.from(document.querySelectorAll("div")).find(o => o.innerText === "Connect to this device"), o => o.click())
}

async function waitForElement(page, getter: () => any, actionFn?: (element: any) => any) {
  let start = Date.now()
  while (Date.now() - start < 5000) {
    const found = await page.evaluate(getter)
    if (found) {
      return actionFn ? await page.evaluate(() => actionFn(found)) : found
    }
    await wait(100)
  }
}

function getTrackLinksOnPage() {
  // @ts-ignore
  return Array.from(document.querySelectorAll(`[data-testid="internal-track-link"]`)).map(o => o.href)
}

function wait(ms: number) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

export type SpotifyTrack = {
  title: string
  id: string
  url: string
  duration: number
  numberInPlaylist: number
  artistNames: string[]
  artistIds: string[]
  originalAlbum: string
  originalAlbumId: string
  thumbnail: string
}

type SpotifyListType = 'library' | 'playlist' | 'album'

type IInterpretResults = {
  tracks: SpotifyTrack[],
  hasMore: boolean,
  limit: number,
  offset: number
}

function interpretLibraryResults(res: any, tracks: SpotifyTrack[]): IInterpretResults {
  const resTracks = res?.data?.me?.library?.tracks || {}
  try {
    resTracks.items.forEach(o => {
      const thumbnails = o.track.data.albumOfTrack.coverArt.sources
      thumbnails.sort((a, b) => b.width - a.width)

      if (o.track.data.playability.playable === false) {
        return
      }

      tracks.push({
        title: o.track.data.name,
        id: o.track._uri.split(':').pop(),
        url: `https://open.spotify.com/track/${o.track._uri.split(':').pop()}`,
        duration: o.track.data.duration.totalMilliseconds,
        numberInPlaylist: o.track.data.track_number,
        artistNames: o.track.data.artists.items.map(o => o.profile.name),
        artistIds: o.track.data.artists.items.map(o => o.uri.split(':').pop()),
        originalAlbum: o.track.data.albumOfTrack.name,
        originalAlbumId: o.track.data.albumOfTrack.uri.split(':').pop(),
        thumbnail: thumbnails[0].url
      })
    })
  } catch (e) {
    console.error(e)
  }

  if (resTracks.pagingInfo.limit + resTracks.pagingInfo.offset >= resTracks.totalCount) {
    return { tracks, hasMore: false, limit: resTracks.pagingInfo.limit, offset: resTracks.pagingInfo.offset }
  }

  return { tracks, hasMore: true, limit: resTracks.pagingInfo.limit, offset: resTracks.pagingInfo.offset }
}

function interpretPlaylistResults(res: any, tracks: SpotifyTrack[]): IInterpretResults {
  const resTracks = res?.data?.playlistV2?.content || {}
  try {
    resTracks.items.forEach(o => {
      const data = o.itemV2.data
      const thumbnails = data.albumOfTrack.coverArt.sources
      thumbnails.sort((a, b) => b.width - a.width)

      tracks.push({
        title: data.name,
        id: data.uri.split(':').pop(),
        url: `https://open.spotify.com/track/${data.uri.split(':').pop()}`,
        duration: data.trackDuration.totalMilliseconds,
        numberInPlaylist: data.trackNumber,
        artistNames: data.artists.items.map(o => o.profile.name),
        artistIds: data.artists.items.map(o => o.uri.split(':').pop()),
        originalAlbum: data.albumOfTrack.name,
        originalAlbumId: data.albumOfTrack.uri.split(':').pop(),
        thumbnail: thumbnails[0].url
      })
    })
  } catch (e) {
    console.error(e)
  }

  if (resTracks.pagingInfo.limit + resTracks.pagingInfo.offset >= resTracks.totalCount) {
    return { tracks, hasMore: false, limit: resTracks.pagingInfo.limit, offset: resTracks.pagingInfo.offset }
  }

  return { tracks, hasMore: true, limit: resTracks.pagingInfo.limit, offset: resTracks.pagingInfo.offset }
}

function interpretAlbumResults(res: any, tracks: SpotifyTrack[]): IInterpretResults {
  const resTracks = res?.data?.albumUnion?.tracksV2 || {}
  try {
    const thumbnails = res.data.albumUnion?.coverArt.sources
    thumbnails.sort((a, b) => b.width - a.width)

    resTracks.items.forEach(o => {
      const data = o.track
      tracks.push({
        title: data.name,
        id: data.uri.split(':').pop(),
        url: `https://open.spotify.com/track/${data.uri.split(':').pop()}`,
        duration: data.duration.totalMilliseconds,
        numberInPlaylist: data.trackNumber,
        artistNames: data.artists.items.map(o => o.profile.name),
        artistIds: data.artists.items.map(o => o.uri.split(':').pop()),
        originalAlbum: res.data.albumUnion?.name,
        originalAlbumId: res.data.albumUnion?.uri.split(':').pop(),
        thumbnail: thumbnails[0].url
      })
    })
  } catch (e) {
    console.error(e)
  }

  return { tracks, hasMore: false, limit: 0, offset: 0 }
}

function getUrlForTargetType(type: SpotifyListType, offset: number = 0, id: string = "") {
  let operation: string
  let variables: string
  let shaHash: string

  if (type === 'library') {
    variables = encodeURIComponent(JSON.stringify({ offset, limit: 2000 }))
    operation = 'fetchLibraryTracks'
    shaHash = '1cb5df9343e3e11ecca539ee85621136f8c1226768a9b7641012c4e6a2339872'
  } else if (type === 'playlist') {
    variables = encodeURIComponent(JSON.stringify({ uri: `spotify:playlist:${id!}`, offset, limit: 2000 }))
    operation = 'fetchPlaylistWithGatedEntityRelations'
    shaHash = '19ff1327c29e99c208c86d7a9d8f1929cfdf3d3202a0ff4253c821f1901aa94d'
  } else {
    variables = encodeURIComponent(JSON.stringify({ uri: `spotify:album:${id!}`, locale: '', offset, limit: 2000 }))
    operation = 'getAlbum'
    shaHash = '8f4cd5650f9d80349dbe68684057476d8bf27a5c51687b2b1686099ab5631589'
  }

  return `https://api-partner.spotify.com/pathfinder/v1/query?operationName=${operation}&variables=${variables}&extensions=%7B%22persistedQuery%22%3A%7B%22version%22%3A1%2C%22sha256Hash%22%3A%22${shaHash}%22%7D%7D`
}

async function loadEntirePlaylist(page: Page, clientToken: string, bearerToken: string, type: SpotifyListType, id: string, offset = 0, tracks: SpotifyTrack[] = []): Promise<SpotifyTrack[]> {
  while (true) {
    const url = getUrlForTargetType(type, offset, id)
    const fetchStr = `fetch('${url}', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'client-token': '${clientToken}',
          'Authorization': '${bearerToken}'
        }
      }).then(res => res.json())`

    console.log(fetchStr)

    const res = await page.evaluate(fetchStr) as any

    const results: IInterpretResults =
      type === 'library' ? interpretLibraryResults(res, tracks) :
        type === 'playlist' ? interpretPlaylistResults(res, tracks,) :
          interpretAlbumResults(res, tracks)

    if (!results.hasMore) {
      break
    }

    await wait(2000)
    offset += results.limit
  }

  return tracks
}

// async function initialise() {
//   //@ts-ignore
//   browser = await launch(puppeteer, {
//     executablePath: process.env.CHROME_LOCATION || '/usr/bin/google-chrome-stable',
//     defaultViewport: {
//       width: 1920,
//       height: 1080,
//     },
//     args: ['--no-sandbox'],
//     //@ts-ignore
//     headless: 'new'
//   });

//   const cookies: Partial<Cookie>[] = [
//     { name: 'sp_dc', value: process.env.SPOTIFY_COOKIE, 'domain': '.spotify.com', 'path': '/', 'expires': 2175434943, 'httpOnly': true, 'secure': true },
//   ]

//   const page = await browser.newPage()

//   console.log(`Page opened`)

//   await page.setCookie(...cookies as any[])
//   await page.goto('https://open.spotify.com')
//   console.log(`Page loaded`)
//   await wait(10000)
// }

// initialise()