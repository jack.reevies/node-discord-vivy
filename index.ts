import dotenv from 'dotenv';
import { Message, VoiceState } from 'discord.js';
dotenv.config();

import aifart from './commands/aifart';
import queue from './commands/queue';
import play from './commands/play';
import playlist from './commands/playlist';
import clear from './commands/clear';
import skip from './commands/skip';
import restart from './commands/restart';
import join from './commands/join';
import stop from './commands/stop';
import pause from './commands/pause';
import resume from './commands/resume';
import jump from './commands/jump';
import shuffle from './commands/shuffle';
import cookie from './commands/cookie';
import { createPlayer, findVCUserIsIn, getChannel, getPlayer, getPlayerInServer, getServerFromChannelId, getUser, login, onDiscordMessage } from './discord';
import { Player, precacheResources } from './player';
import { ErrorType, onError } from './error';
import rename from './commands/rename';
import loop from './commands/loop';
import help from './commands/help';
import reset from './commands/reset';
import neverLonely from './commands/247';
import meme from './commands/meme';
import cowbell from './commands/cowbell';
import file from './commands/file';
import find from './commands/find';
import here from './commands/here';
import download from './commands/download';
import local from './commands/local';
import potoken from './commands/potoken';
import { startTasks } from './tasks';
import say from './commands/say';
import listen from './commands/listen';
import agent from './commands/agent';
import system from './commands/system';
import { create } from './openai';
import { startDbConnection } from './db';
import { get, set } from './db/value';
import { writeFileSync } from 'fs';
import { convertCookieStringToYTDLPFile } from './ytdlp';
import video from './commands/video';
import { refreshCacheList } from './cache';
import seek from './commands/seek';

process.on('unhandledRejection', (reason, promise) => {
  onError(reason as Error, ErrorType.UNEXPECTED_REJECTION)
})

process.on('uncaughtException', (err, origin) => {
  onError(err, ErrorType.UNCAUGHT_EXCEPTION)
})

async function getCookieFromDb() {
  const cookie = await get("cookie")

  if (cookie?.value) {
    writeFileSync('cookies.txt', convertCookieStringToYTDLPFile(cookie.value))
    process.env.YOUTUBE_COOKIE = cookie.value
  }
}

setInterval(getCookieFromDb, 1000 * 60 * 60)

const discordIds = {}
const bannedIds = {}
const infractionMessages = [
  `Sorry, I'm only responding to messages posted in <#${process.env.BOT_COMMAND_CHANNEL_ID}>`,
  `Keep the server clean by keeping bot commands in the <#${process.env.BOT_COMMAND_CHANNEL_ID}> channel`,
  `Use the <#${process.env.BOT_COMMAND_CHANNEL_ID}> channel for bot commands`,
  `Please use the <#${process.env.BOT_COMMAND_CHANNEL_ID}> channel for bot commands`,
  `No. <#${process.env.BOT_COMMAND_CHANNEL_ID}> please`
]

const aggressiveMessages = [
  `Are you actually retarded? I've told you before to use the <#${process.env.BOT_COMMAND_CHANNEL_ID}> channel for bot commands`,
  `You're a bad person`,
  `Fak yuu bluddy`,
  `( ͡⚆ ͜ʖ ͡⚆)╭∩╮`,
  `╭∩╮( ͡° ل͟ ͡° )╭∩╮`,
  `( ° ͜ʖ͡°)╭∩╮`
]

onDiscordMessage(async (message: Message) => {
  const vivyRegex = process.env.COMMAND_PREFIX ? new RegExp(`^${process.env.COMMAND_PREFIX}`, "i") : /^(!|yeet)/i
  const playRegex = /^(play|p|q) /i
  const clearRegex = /^(clear|c) ?$/i
  const skipRegex = /^(skip|s) ?$/i
  const queueRegex = /^(queue|q|list)$/i
  const joinRegex = /^(summon|join|hello|vivy|hi) ?$/i
  const playlistRegex = /^(playlist|pl|list) /i
  const stopRegex = /^(stop|end|shut up) ?$/i
  const pauseRegex = /^(pause|wait|hold) ?$/i
  const unpauseRegex = /^(resume|play|continue)$/i
  const jumpRegex = /^(jump|st|skip) (\d+)/i
  const shuffleRegex = /^(shuffle|sh)$/i
  const loopRegex = /^(loop|repeat)/i
  const renameRegex = /^rename /i
  const saveRegex = /^save /i
  const helpRegex = /^help/i
  const resetRegex = /^(reset|leave|kill|rejoin|fix)/i
  const memeRegex = /^(meme|sfx|fart)/i
  const channelRegex = /(^\d{18}) (\d{18})/i
  const iamRegex = /^iam(\d+)/i
  const cowbellRegex = /^(cow|cowbell)/i
  const fileRegex = /^file/
  const findRegex = /^find/
  const hereRegex = /^here/
  const spotifyRegex = /^download /i

  if (!message.content.match(vivyRegex)) {
    // This is not a vivy command
    return
  }

  if (message.guildId === '468855068964421633' && !message.author.bot && [process.env.DEV_CHANNEL_ID, process.env.BOT_COMMAND_CHANNEL_ID].indexOf(message.channelId) === -1) {
    // Vivy only responds to messages from the bot commands channel or her own dev channel
    const infractionCount = discordIds[message.author.id] || 0
    if (infractionCount < infractionMessages.length) {
      const reply = await message.reply(infractionMessages[infractionCount])
      setTimeout(async () => {
        try {
          message.delete()
          reply.delete()
        } catch (e) { }
      }, 30 * 1000)
      discordIds[message.author.id] = infractionCount + 1
      return
    }

    const randomAggressiveMessage = aggressiveMessages[Math.floor(Math.random() * aggressiveMessages.length)]
    const reply = await message.reply(randomAggressiveMessage)
    setTimeout(async () => {
      try {
        message.delete()
        reply.delete()
      } catch (e) { }
    }, 10 * 1000)
    return;
  }

  let command = message.content.replace(vivyRegex, "").trim()
  let overrideVC: string | null = null
  let overrideTextChannelId: string | null = null

  if (message.author.id === (process.env.OWNER_ID || '') && command.match(channelRegex)) {
    const reg = channelRegex.exec(command)
    if (reg && reg.length > 2) {
      const first = getChannel(reg[1])
      const second = getChannel(reg[2])

      command = command.replace(`${reg[1]} ${reg[2]} `, '')
      command = command.replace(`${reg[1]} ${reg[2]}`, '')

      if (first?.isVoice && second?.isText) {
        overrideVC = reg[1]
        overrideTextChannelId = reg[2]
      } else if (first?.isText && second?.isVoice) {
        overrideVC = reg[2]
        overrideTextChannelId = reg[1]
      } else {
        message.reply(`Looks like you tried to use an admin override but failed to specify voice and text channel IDs.\r\nPlease use \`${process.env.COMMAND_PREFIX}<VOICE_ID> <TEXT_ID>\``)
      }
    } else {
      console.error(`Shouldn't ever get here (Admin Regex matched message but couldnt be exec'd) (${command})`)
    }
  }

  if (message.author.id === (process.env.OWNER_ID || '') && command.match(iamRegex)) {
    const reg = iamRegex.exec(command)

    if (!reg || reg.length < 2) {
      message.reply(`Looks like you tried to use an admin iam override but failed to a user id\r\nPlease use \`${process.env.COMMAND_PREFIX}iam<USER_ID>\``)
      return
    }

    const user = getUser(reg[1])
    if (!user) {
      message.reply(`Failed to lookup user. Was the command correct?\r\nPlease use \`${process.env.COMMAND_PREFIX}iam<USER_ID>\``)
      return
    }

    const vc = findVCUserIsIn(user.id)

    if (!vc) {
      message.reply(`User does not appear to be in a VC`)
      return
    }

    const player = getPlayerInServer(vc.guildId)

    if (!player) {
      message.reply(`We don't have a Player in this server yet. Please use Please use \`${process.env.COMMAND_PREFIX}<VOICE_ID> <TEXT_ID>\` instead`)
      return
    }

    overrideVC = vc.id
    overrideTextChannelId = player.invokedFromChannelId

    command = command.replace(`iam${user.id} `, "")
  }

  const voiceChannelId = overrideVC || message.member?.voice.channelId
  const textChannelId = overrideTextChannelId || message.channelId

  if (!voiceChannelId) {
    // Cannot issue commands when you're not in a VC
    message.reply(`If you're going to subject others to your music, you must be in that VC first`)
    return
  }

  let player: Player | undefined = getPlayer(voiceChannelId)

  if (!player || player.connection.state.status === 'destroyed' || player.connection.state.status === 'disconnected') {
    if (!message.guildId) {
      // We can't save here - but I've never seen a case where guildId is null
      const error = new Error(`Failed to join VC because I can't access this server's guildId`)
      message.reply(`Sorry - ${error.message}. I'm yet to see this occur in the wild, so please report it. Feel free to try again in a little while`)
      return
    }
    // Vivy has not joined this VC yet - lets do that now
    // Potentially a problem where a user in a different VC in the same server issues this command
    // Vivy can only be in one channel on a server at once - do we disconnect?
    // Also, do we transfer their Player if they are?
    player = createPlayer(voiceChannelId, textChannelId)
  }

  if (!player) {
    const error = new Error(`I couldn't find an active session and failed to join your VC`)
    message.reply(`Sorry - ${error.message} - this shouldn't happen.`)
    return
  }

  if (command.match(helpRegex)) {
    await help(player, message)
  } else if (command.startsWith("seek ") || command.startsWith('ff ')) {
    seek(player, message)
  } else if (command.startsWith("video")) {
    // await video(player, message)
  } else if (command.startsWith("aifart")) {
    await aifart(player, message)
  } else if (command.startsWith("agent")) {
    await agent(player, message)
  } else if (command.startsWith("listen")) {
    await listen(player, message)
  } else if (command.startsWith("system")) {
    await system(player, message)
  } else if (command.match(loopRegex)) {
    await loop(player, message, command)
  } else if (command.match(renameRegex)) {
    await rename(player, message, command)
  } else if (command.match(shuffleRegex)) {
    await shuffle(player, message)
  } else if (command.match(jumpRegex)) {
    await jump(player, message, command)
  } else if (command.match(stopRegex)) {
    await stop(player, message)
  } else if (command.match(pauseRegex)) {
    await pause(player, message)
  } else if (command.match(unpauseRegex)) {
    await resume(player, message)
  } else if (command.match(playRegex)) {
    await play(player, message, command)
  } else if (command.match(playlistRegex)) {
    await playlist(player, message, command)
  } else if (command.match(clearRegex)) {
    await clear(player, message)
  } else if (command.match(skipRegex)) {
    await skip(player, message)
  } else if (command.match(queueRegex)) {
    await queue(player, message.channelId)
  } else if (command.startsWith("restart")) {
    await restart(message)
  } else if (command.match(joinRegex)) {
    await join(player, message)
  } else if (command.startsWith("cookie")) {
    await cookie(player, message, command)
  } else if (command.match(resetRegex)) {
    await reset(player, message)
  } else if (command === '247') {
    await neverLonely(player, message)
  } else if (command.match(memeRegex)) {
    await meme(player, message, command)
  } else if (command.match(cowbellRegex)) {
    await cowbell(player, message, command)
  } else if (command.match(fileRegex)) {
    await file(player, message, command)
  } else if (command.match(findRegex)) {
    await find(player, message, command)
  } else if (command.match(hereRegex)) {
    await here(player, message)
  } else if (command.match(spotifyRegex)) {
    await download(player, message, command)
  } else if (command.startsWith("local")) {
    await local(player, message, command)
  } else if (command.startsWith("potoken")) {
    await potoken(player, message, command)
  } else if (command.startsWith("say")) {
    await say(player, message)
  } else {
    message.reply(`Sorry! I don't understand this command. Message <@${process.env.OWNER_ID}> if you have a feature request`)
  }
})

function checkEnvVars() {
  const requiredVars = [
    'DISCORD_CLIENT_TOKEN',
    'MONGO_CONN',
  ]
  const optionalVars = [
    'DEV_CHANNEL_ID',
    'YOUTUBE_COOKIE',
    'IDLE_TIMEOUT_MINUTES',
    'COMMAND_PREFIX',
    'OWNER_ID'
  ]
  const optionalDefaults = [
    '',
    '',
    '30',
    '!'
  ]
  for (let i = 0; i < requiredVars.length; i++) {
    const name = requiredVars[i];
    if (!process.env[name]) {
      console.error(`Required env var ${name} is missing - cannot continue`)
      setTimeout(() => process.exit(1), 1)
    }
  }

  for (let i = 0; i < optionalVars.length; i++) {
    const name = optionalVars[i];
    const val = optionalDefaults[i];
    if (!process.env[name]) {
      process.env[name] = val
      console.error(`Optional env var ${name} is missing - using default ${val}`)
    }
  }
}

async function start() {
  checkEnvVars()
  try {
    await startDbConnection()
    await precacheResources()
    await login()
    await refreshCacheList()
    setInterval(refreshCacheList, 1000 * 60)
    startTasks()
    console.log('Ready!');
  }
  catch (e) {
    // Initial logon failed, now what?
    setTimeout(() => process.exit(1), 5000)
  }
}

start()