# Vivy

A replacement for the now defunct Groovy bot. 

## Motivation

Groovy was shut down on 30th August 2021 after a Cease and Desist order from Google. Groovy was certainly the "go-to" music bot for discord servers having been installed on over 16 million server. Alternatives such as Rhythm exist, however, they have a fundamental drawback of not being able to play age restricted Youtube videos. There are also a lot of pre-baked modules out there that exhibit the same limitation. This is not acceptable when YouTube videos can get age restricted based on abitrary rules.

**EDIT:** Rhythm is also being shut down on the 15th September as well. Yikes.

## Features

The most basic features of Groovy are covered.

Feature | Status | Command | Example
-|-|-|-
Add single video to queue | Y | !play, !p | !p https://www.youtube.com/watch?v=_ynaX7PtJe0  
Add playlist to queue | Y | !playlist, !list | !list https://www.youtube.com/playlist?list=PLw0UrUlz9vFpjaXKphZW53TDTMgB8INpM
Skip | Y | !skip, !s | !skip
Skip To | Y | !jump, !skip, !st | !jump 69
Stop | Y | !stop, !end, !quit, !shut up | !stop
Shuffle | Y | !shuffle, !sh | !shuffle
Clear | Y | !clear, !c | !clear
Set a new Cookie | Y | !cookie | !cookie YOUTUBECOOKIE

## Planned Features

#### Self Authentication

Currently Vivy accepts any YouTube cookie to be used for playing age restricted video, but cannot get one herself. I don't know how long cookies are valid for (the current one has been going since proejct start - 6 days ago), but regardless, it will expire, and it will be a pain to have to provide a new one every so often. If Vivy could get this automatically, it would go a long way for automation

#### Spotify support

Groovy *couldn't* do this, instead it would take a spotify playlist and try to find the matching video on YouTube to play. But why stop there? Surely it can be done

**EDIT:** After looking into it, theres a ton of DRM and encryption around Spotify streams. Adding native support would mean breaking their encryption, which would be illegal. So best not to since even Groovy and Rhythm got into trouble. I will look into replicating Groovy's support for looking up playlists from Spotify on YouTube though 

#### Role based DJ

Server owners can restrict people from using commands, for example, !djonly makes it so that only members with the "DJ" role can add/skip songs

#### Slash commands

Our communities don't tend to use these, but they're officially supported by discord and allow for some nice self discovery with descriptions. So whats the harm, more options is better right?

#### Customizable commands 

Vivy currently listens to commands prefixed with "!" or "YEET". It would be nice for individual servers to customize this.

#### Seek

I didn't personally use this feature much in Groovy, but the ability to skip back/forward X seconds in tracks would be useful

#### Music Effects

Groovy and Rhythm had these features locked behind a paywall. The ability to "nightcore", "speed up", "slow down" music. I never used these even though I paid for premium. But they are fun. Why not?
