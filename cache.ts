import { sep } from "path";
import { getDurationFormatted, getLocalFiles, noFail, Track } from "./helpers";
import sanitize from "sanitize-filename";
import { Type } from "./queue";
const { getAudioDurationInSeconds } = require('get-audio-duration');

const cacheFiles = {
  local: [],
  cache: []
}

export async function refreshCacheList() {
  cacheFiles.cache = await getLocalFiles(false)
  cacheFiles.local = await getLocalFiles(true)
}

export function getCache() {
  return cacheFiles
}

export function getNameForCache(cacheAlias: string, source?: Type, ext?: string) {
  if (ext) {
    return `${process.env.CACHE_PATH}/${sanitize(cacheAlias)}.${ext}`
  }
  return `${process.env.CACHE_PATH}/${sanitize(cacheAlias)}.${source === Type.SOUNDCLOUD || source === Type.BANDCAMP ? 'mp3' : 'ogg'}`
}

export function findInCache(alias: string) {
  alias = alias.replace('.wav', '').replace('.mp3', '').replace('.ogg', '')
  return cacheFiles.local.find((o: string) => {
    const name = o.split(sep).pop()
    const nameNoExt = name.split('.').slice(0, -1).join('.')
    return alias.toLowerCase() === nameNoExt.toLowerCase()
  })
}

async function findCache(identifier: string) {
  // First attempt find by Id
  const idMatch = cacheFiles.local.find(o => o.toLowerCase().split('.').slice(0, -1).join('.').endsWith(`[${identifier.toLowerCase()}]`))

  if (idMatch) {
    return idMatch
  }

  // Else match by full text?

  const textMatch = cacheFiles.local.find(o => o.toLowerCase().split('.').slice(0, -1).join('.').endsWith(`${identifier.toLowerCase()}`))

  if (textMatch) {
    return textMatch
  }

  //return cacheFiles.local.find(o => o.toLowerCase().includes(identifier.toLowerCase()))
}

export async function loadFromCache(identifier: string): Promise<Track | undefined> {
  const cacheItem = await findCache(identifier)
  if (!cacheItem) return

  const path = (cacheItem as string).split(sep).slice(0, -1).join(sep)
  const fileName = (cacheItem as string).split(sep).pop()
  const reversed = fileName.split('').reverse().join('')

  const idRegex = /(\].*?\[)(.*)/g.exec(reversed)

  if (!idRegex) {
    return undefined
  }

  const id = idRegex?.[1].split('').reverse().join('')
  const nameNoId = idRegex?.[2].split('').reverse().join('').trim()
  const fileNameNoExt = reversed.substring(idRegex.index).split('').reverse().join('')

  if (!id || !path) {
    return undefined
  }

  const secondDuration = Math.round((await noFail(() => getAudioDurationInSeconds(cacheItem)) || 0))

  return {
    title: nameNoId.trim(),
    duration: secondDuration,
    durationFormatted: getDurationFormatted(secondDuration),
    url: `https://example.com/${id.substring(1, id.length - 2)}`,
    cacheAlias: fileNameNoExt
  }
}
