import { sep } from "path";
import { ensurePathExists, joinArr, singularInterval, wait } from "./helpers";
import { SpotifyTrack, currentlyPlaying, getListTracks, getSpotifyStream } from "./spotifyWeb";
import sanitize from "sanitize-filename";
import { createWriteStream, existsSync, renameSync, rmSync, statSync, writeFileSync } from "fs";
import { convertOggToMp3, removeLeadingSilence, verifyffmpegLength } from "./ffmpeg";

export function getTrackFileName(track: SpotifyTrack) {
  return `${track.title} - ${joinArr(track.artistNames, ' ', 32) || track.artistNames[0]}`
}

export async function watchSpotifyLibrary() {
  const folder = process.env.SPOTIFY_DOWNLOAD_DIR
  ensurePathExists(folder)
  const library = await getListTracks('https://open.spotify.com/collection/tracks')

  await fixOlderTracks(library.tracks)

  for (const track of library.tracks) {
    const fileName = getTrackFileName(track)
    const completeName = `${folder}${sep}${sanitize(fileName)}`

    if (existsSync(completeName + '.mp3')) {
      console.log(`Skipping ${track.title} because it already exists`)
      cleanupTempFiles(completeName)
      continue
    }

    try {
      await downloadSpotifyTrack(track, completeName)
      console.log(`Downloaded ${track.title}`)
    }
    catch (e) {
      console.error(e)
    }
  }
}

export function cleanupTempFiles(fileName: string) {
  try { rmSync(fileName + '.jpg') } catch (e) { }
  try { rmSync(fileName + '.ogg') } catch (e) { }
  try { rmSync(fileName + '.temp.mp3') } catch (e) { }
}

async function fixOlderTracks(tracks: SpotifyTrack[]) {
  const folder = `${process.env.CACHE_PATH}${sep}Liked Songs`
  for (const track of tracks) {
    const fileName = getTrackFileName(track)
    const completeName = `${folder}${sep}${sanitize(fileName)}`

    if (existsSync(completeName + '.mp3')) {
      const stat = statSync(completeName + '.mp3')
      const needsTrim = stat.ctimeMs < new Date(2024, 13, 16, 6, 0, 0).getTime()

      if (!needsTrim) {
        console.log(`Skipping ${track.title} because it already exists and has silence removed and has year`)
        cleanupTempFiles(completeName)
        continue
      }

      const { artist, album, year } = await getSpotifyStream(track.url, true)

      const tags = {
        title: track.title,
        originalTitle: track.title,
        originalArtist: artist,
        performerInfo: artist,
        artist: artist,
        album: album,
        date: year || '1900',
        involvedPeopleList: track.artistNames.join(','),
        thumb: fileName + '.jpg',
        year: year || '1900',
      }

      renameSync(completeName + '.mp3', completeName + '.mp3.temp')
      await removeLeadingSilence(completeName + '.mp3.temp', completeName + '.mp3', tags)
    }
  }
}

export async function downloadSpotifyTrack(track: SpotifyTrack, fileName: string) {
  const download = async () => {
    const { stream, artist, album, year, thumb } = await getSpotifyStream(track.url, false, (data) => {
      if (!seconds) {
        seconds = data.duration
      }
    })

    const fileStream = createWriteStream(`${fileName}.ogg.partial`)
    stream.pipe(fileStream) as any as NodeJS.ReadStream
    let seconds = 0

    await new Promise(async resolve => {
      stream.on('end', () => {
        fileStream.end()
        fileStream.close()

        if (statSync(`${fileName}.ogg.partial`).size > 1024 * 50) {
          renameSync(`${fileName}.ogg.partial`, `${fileName}.ogg`)
        }
        console.log(`Finished writing to file`)
        resolve(fileName)
      })
    })

    if (!seconds) {
      throw new Error(`Never got duration for ${track.title}`)
    }

    const res = await fetch(thumb)
    const arrayBuffer = await res.arrayBuffer()
    const buffer = Buffer.from(arrayBuffer)
    writeFileSync(`${fileName}.jpg`, buffer)

    const tags = {
      title: track.title,
      originalTitle: track.title,
      originalArtist: artist,
      performerInfo: artist,
      artist: artist,
      album: album,
      date: year || '1900',
      involvedPeopleList: track.artistNames.join(','),
      thumb: fileName + '.jpg',
      year: year || '1900',
    }

    await convertOggToMp3(fileName + '.ogg', fileName + '.temp.mp3', tags)
    // Doing this check before removing silence because the track could start with a long silence intentionally that would then break the length check
    await verifyffmpegLength(fileName + '.temp.mp3', seconds)
    await removeLeadingSilence(fileName + '.temp.mp3', fileName + '.mp3')
  }

  return await download().catch(e => console.error(e)).finally(() => cleanupTempFiles(fileName))
}