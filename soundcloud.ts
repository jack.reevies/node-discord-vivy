import { getDurationFormatted, Track } from "./helpers"
// @ts-ignore
import fetch from 'node-fetch';

const scdl = require('soundcloud-downloader').default

export async function getInfo(url: string) {
  try {
    const info = await scdl.getInfo(url)
    const length = Math.round(info.duration / 1000)
    return {
      cacheAlias: `${info.title.substring(0, 100)} [${info.id}]`,
      url: info.permalink_url,
      title: info.title,
      duration: length,
      durationFormatted: getDurationFormatted(length)
    } as Track
  } catch (e) {
    return false
  }
}

export async function getPlaylistTracks(url: string) {
  try {
    const playlist = await getBasePlaylist(url)
    if (!playlist) return [] as Track[]
    const tracks = await lookupTracks(playlist.tracks)
    return tracks.map((o: any) => {
      const length = Math.round(o.duration / 1000)
      return {
        cacheAlias: `${o.title.substring(0, 100)} [${o.id}]`,
        title: o.title,
        duration: length,
        durationFormatted: getDurationFormatted(length),
        url: o.permalink_url
      } as Track
    }) as Track[]
  } catch (e) {
    return [] as Track[]
  }
}

async function getBasePlaylist(url: string) {
  const res = await fetch(url)
  const text = await res.text()
  const json = extractHydratableScript(text)
  const playlist = json.find((o: any) => o.hydratable === 'playlist')
  return playlist ? playlist.data : null
}

function extractHydratableScript(html: string) {
  const hydratableTag = 'window.__sc_hydration ='
  html = html.substring(html.indexOf(hydratableTag) + hydratableTag.length)
  html = html.substring(0, html.indexOf(';</script>'))
  const json = JSON.parse(html)
  return json
}

async function lookupTracks(json: { id: number }[]) {
  const ids = json.map(o => o.id.toString()).join('%2C')
  const url = `https://api-v2.soundcloud.com/tracks?ids=${ids}&client_id=JpcTFmpEz9lMPDrM6TDAC9izag7Be06D&limit=25&offset=0&linked_partitioning=1&app_version=1641477557&app_locale=en`
  const res = await fetch(url)
  return res.json()
}