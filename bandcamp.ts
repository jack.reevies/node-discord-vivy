import { Readable } from "stream"
import { BandcampAlbum } from "./types/bandcamp"

export async function getTracksFromBandcamp(url: string) {
  const res = await fetch(url)
  if (res.status !== 200) {
    console.error(`Bandcamp request failed with status ${res.status}`)
    return []
  }

  const text = await res.text()

  const tralbumRaw = /data-tralbum="(.+?)"/g.exec(text)?.[1]?.replaceAll('&quot;', '"')
  const tralbum: BandcampAlbum = JSON.parse(tralbumRaw)

  return tralbum.trackinfo.map(o => {
    return {
      id: o.id,
      title: o.title,
      artist: o.artist,
      albumUri: tralbum.album_url,
      duration: o.duration,
      uri: o.file['mp3-128'] || Object.keys(o.file)[0]
    }
  })
}

export async function getBandcampStream(url: string) {
  return Readable.fromWeb(((await fetch(url)).body as any))
}