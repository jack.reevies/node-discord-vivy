export type YTDLPFormat = Format & {
  itag: number
  bitrate?: number
  isLive: boolean
  hasAudio: boolean
  hasVideo: boolean
  approxDurationMs: string
  container: string
  audioBitrate?: number
  initSegmentUri?: string
  initSegmentUris?: string
  mimeType: string
  qualityLabel: string
  size?: number
  audioChannels?: number
  audioCodec: string
  audioQuality: string
  lastModified: string
}

export interface YTDLPVideoInfo {
  id: string
  title: string
  formats: Format[]
  thumbnails: Thumbnail[]
  thumbnail: string
  description: string
  channel_id: string
  channel_url: string
  duration: number
  view_count: number
  average_rating: any
  age_limit: number
  webpage_url: string
  categories: string[]
  tags: string[]
  playable_in_embed: boolean
  live_status: string
  release_timestamp: any
  _format_sort_fields: string[]
  automatic_captions: Record<string, {
    ext: string
    url: string
    name: string
  }>
  subtitles: Record<string, [{
    ext: string
    url: string
    name: string
  }]>
  comment_count: number
  chapters: Chapter[]
  heatmap: any
  like_count: number
  channel: string
  channel_follower_count: number
  channel_is_verified: boolean
  uploader: string
  uploader_id: string
  uploader_url: string
  upload_date: string
  timestamp: number
  availability: string
  original_url: string
  webpage_url_basename: string
  webpage_url_domain: string
  extractor: string
  extractor_key: string
  playlist: any
  playlist_index: any
  display_id: string
  fulltitle: string
  duration_string: string
  release_year: any
  is_live: boolean
  was_live: boolean
  requested_subtitles: any
  _has_drm: any
  epoch: number
  asr: number
  filesize: number
  format_id: string
  format_note: string
  source_preference: number
  fps: number
  audio_channels: number
  height: number
  quality: number
  has_drm: boolean
  tbr: number
  filesize_approx: number
  url: string
  width: number
  language: string
  language_preference: number
  preference: any
  ext: string
  vcodec: string
  acodec: string
  dynamic_range: string
  downloader_options: DownloaderOptions2
  protocol: string
  video_ext: string
  audio_ext: string
  vbr: any
  abr: any
  resolution: string
  aspect_ratio: number
  http_headers: HttpHeaders2
  format: string
  _filename: string
  filename: string
  _type: string
  _version: Version
}

export interface Format {
  format_id: string
  format_note: string
  ext: string
  protocol: string
  acodec: string
  vcodec: string
  url: string
  width?: number
  height?: number
  fps?: number
  rows?: number
  columns?: number
  fragments?: Fragment[]
  audio_ext: string
  video_ext: string
  vbr?: number
  abr?: number
  tbr?: number
  resolution: string
  aspect_ratio?: number
  filesize_approx?: number
  http_headers: HttpHeaders
  format: string
  asr?: number
  filesize?: number
  source_preference?: number
  audio_channels?: number
  quality?: number
  has_drm?: boolean
  language?: string
  language_preference?: number
  preference: any
  dynamic_range?: string
  container?: string
  downloader_options?: DownloaderOptions
}

export interface Fragment {
  url: string
  duration: number
}

export interface HttpHeaders {
  "User-Agent": string
  Accept: string
  "Accept-Language": string
  "Sec-Fetch-Mode": string
}

export interface DownloaderOptions {
  http_chunk_size: number
}

export interface Thumbnail {
  url: string
  preference: number
  id: string
  height?: number
  width?: number
  resolution?: string
}

export interface Chapter {
  start_time: number
  title: string
  end_time: number
}

export interface DownloaderOptions2 {
  http_chunk_size: number
}

export interface HttpHeaders2 {
  "User-Agent": string
  Accept: string
  "Accept-Language": string
  "Sec-Fetch-Mode": string
}

export interface Version {
  version: string
  current_git_head: any
  release_git_head: string
  repository: string
}
