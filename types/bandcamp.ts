export interface BandcampAlbum {
  'for the curious': string;
  current: Current;
  preorder_count?: any;
  hasAudio: boolean;
  art_id: number;
  packages: any[];
  defaultPrice: number;
  freeDownloadPage?: any;
  FREE: number;
  PAID: number;
  artist: string;
  item_type: string;
  id: number;
  last_subscription_item?: any;
  has_discounts: boolean;
  is_bonus?: any;
  play_cap_data?: any;
  is_purchased?: any;
  items_purchased?: any;
  is_private_stream?: any;
  is_band_member?: any;
  licensed_version_ids?: any;
  package_associated_license_id?: any;
  has_video?: any;
  tralbum_subscriber_only: boolean;
  album_is_preorder: boolean;
  album_release_date: string;
  trackinfo: Trackinfo[];
  playing_from: string;
  album_url: string;
  album_upsell_url: string;
  url: string;
}

export interface Trackinfo {
  id: number;
  track_id: number;
  file: File;
  artist: string;
  title: string;
  encodings_id: number;
  license_type: number;
  private?: any;
  track_num: number;
  album_preorder: boolean;
  unreleased_track: boolean;
  title_link: string;
  has_lyrics: boolean;
  has_info: boolean;
  streaming: number;
  is_downloadable: boolean;
  has_free_download?: any;
  free_album_download: boolean;
  duration: number;
  lyrics?: any;
  sizeof_lyrics: number;
  is_draft: boolean;
  video_source_type?: any;
  video_source_id?: any;
  video_mobile_url?: any;
  video_poster_url?: any;
  video_id?: any;
  video_caption?: any;
  video_featured?: any;
  alt_link?: any;
  encoding_error?: any;
  encoding_pending?: any;
  play_count?: any;
  is_capped?: any;
  track_license_id?: any;
}

export interface File {
  'mp3-128': string;
}

export interface Current {
  audit: number;
  title: string;
  new_date: string;
  mod_date: string;
  publish_date: string;
  private?: any;
  killed?: any;
  download_pref?: any;
  require_email?: any;
  is_set_price?: any;
  set_price: number;
  minimum_price: number;
  minimum_price_nonzero?: any;
  require_email_0?: any;
  artist: string;
  about?: any;
  credits?: any;
  auto_repriced?: any;
  new_desc_format: number;
  band_id: number;
  selling_band_id: number;
  art_id: number;
  download_desc_id?: any;
  track_number: number;
  release_date?: any;
  file_name?: any;
  lyrics?: any;
  album_id: number;
  encodings_id: number;
  pending_encodings_id?: any;
  license_type: number;
  isrc?: any;
  preorder_download?: any;
  streaming: number;
  id: number;
  type: string;
}