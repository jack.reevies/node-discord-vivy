import { Client, Intents, Message, TextChannel, Collection } from 'discord.js';
import { generateDependencyReport, joinVoiceChannel } from '@discordjs/voice'
import { Player } from './player';
import { QueueItem } from './queue';
import { loadFromCache } from './cache';
import { wait } from './helpers';
import { streamAudio } from './tts';
import { PassThrough } from 'stream';

// Create a new client instance
const client = new Client({
  intents: [
    Intents.FLAGS.GUILDS,
    Intents.FLAGS.GUILD_VOICE_STATES,
    Intents.FLAGS.GUILD_MESSAGES,
    Intents.FLAGS.GUILD_MESSAGE_REACTIONS,
    Intents.FLAGS.MESSAGE_CONTENT
  ],
  partials: ['CHANNEL', 'GUILD_MEMBER', 'MESSAGE', 'REACTION', 'USER']
});

// When the client is ready, run this code (only once)
client.once('ready', () => {
  client.user?.setPresence({
    status: "online",
    activities: [
      {
        name: `${process.env.COMMAND_PREFIX}help for info || ${client.guilds.cache.size} servers`,
        type: "PLAYING",
        url: "https://gitlab.com/jack.reevies/node-discord-vivy"
      }
    ]
  })
  console.log('Ready!');
  console.log(generateDependencyReport())

  client.on('voiceStateUpdate', async (oldState, newState) => {
    const sounds = {
      '339375761029070858': ['ibstrokin', 'microwave', 'schnibblyschnap'], // Me
      '545386162131435530': ['bumpercars', 'bumperfart', '10points'], // Dan
      '336887325336272896': ['iwannacub', 'lamechanophili'], // Maksym
      '279341198068547584': ['aaa'], // SoulStyx
      '128219556350001152': ['ahthelight'], // Curtis
      '374650987937267742': ['betternicecock'], // Anna
      '143452991842811905': ['clapclapclap', 'clapclapfart']
    }

    const fartside = '759074442403119185'

    if (newState.channel?.id === fartside) {
      let player: Player | undefined = getPlayer(fartside)

      if (!player || player.connection.state.status === 'destroyed' || player.connection.state.status === 'disconnected') {
        // Vivy has not joined this VC yet - lets do that now
        // Potentially a problem where a user in a different VC in the same server issues this command
        // Vivy can only be in one channel on a server at once - do we disconnect?
        // Also, do we transfer their Player if they are?
        player = createPlayer(fartside, process.env.BOT_COMMAND_CHANNEL_ID)
      }

      if (!newState.member.user.bot) {
        await wait(2000)
        // Pick random
        const list = sounds[newState.member.id] || ['ogfart2', 'ogfart1', 'ogfart3']
        await player.playMeme(list[Math.floor(Math.random() * list.length)])

        // const choices = [
        //   `You're looking radiant today - ${newState.member.user.username} isn't it lovely to catch up?`,
        //   `Oh shit, it's ${newState.member.user.username}. Hide yo keeds, hide yo wife`,
        //   `Ayyo was good .... ${newState.member.user.username}`,
        // ]

        // try {
        //   const ttsStream = await streamAudio(choices[Math.floor(Math.random() * choices.length)])
        //   const passThrough = new PassThrough()
        //   ttsStream.pipe(passThrough)
        //   player.playStreamAndWait(passThrough as any, `tts-${Date.now()}-join-${newState.member.user.username}`, false, player.cancelToken, 1.0)
        // } catch (e) { console.log(e) }
      }
    }
  })
});

const channels: Record<string, Player | undefined> = {}

export function createPlayer(voiceChannelId: string, invokedFromChannelId: string, initialQueue?: QueueItem[], initialHistory?: QueueItem[], id?: string) {
  const connection = joinVCChannel(voiceChannelId)
  if (!connection) return

  const player = new Player(connection, invokedFromChannelId, initialQueue, initialHistory, id)
  channels[voiceChannelId] = player
  return player
}

export function getPlayerInServer(serverId: string) {
  const server = client.guilds.cache.get(serverId)
  if (!server) return
  const channel = server.channels.cache.find(channel => {
    return Object.keys(channels).indexOf(channel.id) > -1
  })

  if (!channel) return
  return channels[channel.id]
}

export function getPlayer(voiceChannelId: string) {
  return channels[voiceChannelId]
}

export function removePlayer(voiceChannelId: string) {
  channels[voiceChannelId] = undefined
  delete channels[voiceChannelId]
}

export function getChannel(id: string) {
  return client.channels.cache.get(id)
}

export function joinVCChannel(id: string) {
  const channel: any = getChannel(id)
  if (!channel) return

  return joinVoiceChannel({
    channelId: channel.id,
    guildId: channel.guild.id,
    adapterCreator: channel.guild.voiceAdapterCreator,
    selfDeaf: false
  })
}

export function login() {
  // Login to Discord with your client's token
  if (!process.env.DISCORD_CLIENT_TOKEN) {
    console.error(`No token is set - please set this through an environment variable (preferably .env file) and restart this app`)
    return
  }
  return client.login(process.env.DISCORD_CLIENT_TOKEN);
}

export function onDiscordMessage(fn: (message: Message) => any) {
  client.on('messageCreate', async (message) => {
    try { await fn(message) } catch (e) {
      try { await message.reply(`⚠️ Something went wrong! ⚠️`) } catch (e) { }
      console.error(e)
    }
  })
}

export function removeListener(fn: (message: Message) => any) {
  client.removeListener('messageCreate', fn)
}

export async function clearMyMessagesInChannel(channelId: string) {
  const channel = getChannel(channelId) as TextChannel
  if (!channel) return

  const collection = await channel.messages.fetch()
  collection.filter(o => o.author.id === client.application?.id).filter(o => o.embeds.length > 0).each(o => {
    if (o.embeds[0].title?.startsWith("Queue")) {
      try { o.delete() } catch (e: any) {
        console.warn(`failed to delete message ${o.id} because ${e.message}}`)
      }
    }
  })
}

export async function getMessagesInChannel(channelId: string) {
  const channel = getChannel(channelId) as TextChannel
  if (!channel) return new Collection()

  return channel.messages.fetch()
}

export function getServerNameOfChannel(channelId: string) {
  const channel = getChannel(channelId) as TextChannel
  if (!channel) return 'CHANNEL NOT FOUND'

  return channel.guild.name
}

export async function sendMessage(channelId: string, data: Parameters<TextChannel['send']>[0]) {
  const channel = getChannel(channelId) as TextChannel
  if (!channel) return

  return channel.send(data)
}

export function getVCConnectionsInServer(serverId: string) {
  return client.voice.adapters.get(serverId)
}

export function getUser(userId: string) {
  return client.users.cache.find(user => user.id === userId)
}

export function findVCUserIsIn(userId: string) {
  const user = getUser(userId)
  if (!user) return

  for (const [id, channel] of Array.from(client.channels.cache)) {
    if (!channel.isVoice()) continue
    if (channel.members.get(userId)) return channel
  }
}

export function getServerFromChannelId(channelId: string) {
  for (const [id, server] of Array.from(client.guilds.cache)) {
    if (!server) continue
    for (const channelId in server.channels.cache) {
      if (channelId === id) {
        return id
      }
    }
  }
}